package com.elab.openbmsclient.fragment;

import android.content.ActivityNotFoundException;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.net.MailTo;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.GeolocationPermissions;
import android.webkit.ValueCallback;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

import com.elab.openbmsclient.R;
import com.elab.openbmsclient.WebViewAppConfig;
import com.elab.openbmsclient.service.SendDynIDService;
import com.elab.openbmsclient.task.TaskFragment;
import com.elab.openbmsclient.utility.DownloadUtility;
import com.elab.openbmsclient.utility.Logcat;
import com.elab.openbmsclient.utility.NetworkManager;
import com.elab.openbmsclient.utility.display.DisplayToast;
import com.elab.openbmsclient.view.ViewState;


public class WebViewFragment extends TaskFragment implements SwipeRefreshLayout.OnRefreshListener
{
	private static final String ARGUMENT_URL = "url";
	private static final String ARGUMENT_SHARE = "share";
    private static final String ROOMID_DEPENDENT = "roomid_dep";
    private static final String ACCOUNT_DEPENDENT = "account_dep";
	private static final int REQUEST_FILE_PICKER = 1;

	private boolean mActionBarProgress = false;
	private ViewState mViewState = null;
	private View mRootView;
	private String mUrl = "about:blank";
	private String mShare;
	private boolean mLocal = false;
    private boolean mRoomIdDependent = false;
    private boolean mAccountDependent = false;
	private ValueCallback<Uri> mFilePathCallback4;
	private ValueCallback<Uri[]> mFilePathCallback5;

	public static WebViewFragment newInstance(String url, String share, boolean roomid_dep, boolean isaccount_webview)
	{
		WebViewFragment fragment = new WebViewFragment();

		// arguments
		Bundle arguments = new Bundle();
		arguments.putString(ARGUMENT_URL, url);
		arguments.putString(ARGUMENT_SHARE, share);
        arguments.putBoolean(ROOMID_DEPENDENT, roomid_dep);
        arguments.putBoolean(ACCOUNT_DEPENDENT, isaccount_webview);
		fragment.setArguments(arguments);

		return fragment;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		
		setHasOptionsMenu(true);
		setRetainInstance(true);

		// handle fragment arguments
		Bundle arguments = getArguments();
		if(arguments != null)
		{
			handleArguments(arguments);
		}
	}
	
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
	{
		mRootView = inflater.inflate(R.layout.fragment_webview, container, false);
		return mRootView;
	}
	
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState)
	{
		super.onActivityCreated(savedInstanceState);

		// restore webview state
		if(savedInstanceState!=null)
		{
			WebView webView = (WebView) mRootView.findViewById(R.id.fragment_main_webview);
			webView.restoreState(savedInstanceState);
		}

		// setup webview
		renderView();

		// pull to refresh
		SwipeRefreshLayout swipeRefreshLayout = (SwipeRefreshLayout) mRootView.findViewById(R.id.container_swipe_refresh);
		swipeRefreshLayout.setOnRefreshListener(this);

		// load and show data
		if(mViewState==null || mViewState==ViewState.OFFLINE)
		{
			loadData();
		}
		else if(mViewState==ViewState.CONTENT)
		{
			showContent();
		}
		else if(mViewState==ViewState.PROGRESS)
		{
			showProgress();
		}
		else if(mViewState==ViewState.EMPTY)
		{
			showEmpty();
		}
		
		// progress in action bar
		showActionBarProgress(mActionBarProgress);
	}

	@Override
	public void onResume()
	{
		super.onResume();
        if (mRoomIdDependent)
        {
        // Receiver to update the roomid
        this.getActivity().registerReceiver(RoomIDUpdater, new IntentFilter(SendDynIDService.IntentNameNewRoomIDUpdater));
        }
        if (mAccountDependent)
        {
            // Receiver to update the account page
            this.getActivity().registerReceiver(AccountUpdater, new IntentFilter(SendDynIDService.IntentNameNewAccount));
        }
	}
	
	
	@Override
	public void onPause()
	{
		super.onPause();
        if (mRoomIdDependent) {
            //Unregister Receiver
            this.getActivity().unregisterReceiver(RoomIDUpdater);
        }
        if (mAccountDependent) {
            //Unregister Receiver
            this.getActivity().unregisterReceiver(AccountUpdater);
        }
	}
	

	@Override
	public void onDestroyView()
	{
		super.onDestroyView();
		mRootView = null;
	}

	@Override
	public void onSaveInstanceState(Bundle outState)
	{
		// save current instance state
		super.onSaveInstanceState(outState);
		setUserVisibleHint(this.isVisible());

		// save webview state
		WebView webView = (WebView) mRootView.findViewById(R.id.fragment_main_webview);
		webView.saveState(outState);
	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater)
	{
		// action bar menu
		super.onCreateOptionsMenu(menu, inflater);
		inflater.inflate(R.menu.menu_main, menu);

		// show or hide share button
		MenuItem share = menu.findItem(R.id.menu_share);
		share.setVisible(mShare!=null && !mShare.trim().equals(""));
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) 
	{
		// action bar menu behaviour
		switch(item.getItemId())
		{
			case R.id.menu_share:
				startShareActivity(getString(R.string.app_name), mShare);
				return true;

			default:
				return super.onOptionsItemSelected(item);
		}
	}

	@Override
	public void onRefresh()
	{
		runTaskCallback(new Runnable()
		{
			@Override
			public void run()
			{
				refreshData();
			}
		});
	}

	private void handleArguments(Bundle arguments)
	{
		if(arguments.containsKey(ARGUMENT_URL))
		{
			mUrl = arguments.getString(ARGUMENT_URL);
			mLocal = mUrl.contains("file://");
		}
		if(arguments.containsKey(ARGUMENT_SHARE))
		{
			mShare = arguments.getString(ARGUMENT_SHARE);
		}
        if(arguments.containsKey(ROOMID_DEPENDENT))
        {
            mRoomIdDependent = arguments.getBoolean(ROOMID_DEPENDENT);
        }
        if(arguments.containsKey(ACCOUNT_DEPENDENT))
        {
            mAccountDependent = arguments.getBoolean(ACCOUNT_DEPENDENT);
        }
	}

	private void loadData()
	{
		if(NetworkManager.isOnline(getActivity()) || mLocal)
		{
			// show progress
			showProgress();

			// load web url
			WebView webView = (WebView) mRootView.findViewById(R.id.fragment_main_webview);
			webView.loadUrl(mUrl);
		}
		else
		{
			showOffline();
		}
	}

    // Receiver to update the webview when the new room ID is received
    private BroadcastReceiver RoomIDUpdater= new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            // set the url for the fragment
            mUrl = intent.getStringExtra(SendDynIDService.ExtraNameNewUrl);
            loadData();
        }
    };

    // Receiver to update the account webview when the new user id is received
    private BroadcastReceiver AccountUpdater = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            loadData();
        }
    };

	public void refreshData()
	{
		if(NetworkManager.isOnline(getActivity()) || mLocal)
		{
			// show progress in action bar
			showActionBarProgress(true);

			// load web url
			WebView webView = (WebView) mRootView.findViewById(R.id.fragment_main_webview);
			webView.loadUrl(webView.getUrl());
		}
		else
		{
			showActionBarProgress(false);
			Context context = getActivity();
			DisplayToast.createToast(context, context.getString(R.string.NoNetworkToast), Toast.LENGTH_LONG, 1);
		}
	}


	private void showActionBarProgress(boolean visible)
	{
		// show pull to refresh progress bar
		SwipeRefreshLayout swipeRefreshLayout = (SwipeRefreshLayout) mRootView.findViewById(R.id.container_swipe_refresh);

		swipeRefreshLayout.setRefreshing(visible);
		swipeRefreshLayout.setEnabled(!visible);

		mActionBarProgress = visible;
	}
	
	
	private void showContent()
	{
		// show content container
		ViewGroup containerContent = (ViewGroup) mRootView.findViewById(R.id.container_content);
		ViewGroup containerProgress = (ViewGroup) mRootView.findViewById(R.id.container_progress);
		ViewGroup containerOffline = (ViewGroup) mRootView.findViewById(R.id.container_offline);
		ViewGroup containerEmpty = (ViewGroup) mRootView.findViewById(R.id.container_empty);
		containerContent.setVisibility(View.VISIBLE);
		containerProgress.setVisibility(View.GONE);
		containerOffline.setVisibility(View.GONE);
		containerEmpty.setVisibility(View.GONE);
		mViewState = ViewState.CONTENT;
	}


	private void showContent(final long delay)
	{
		final Handler timerHandler = new Handler();
		final Runnable timerRunnable = new Runnable()
		{
			@Override
			public void run()
			{
				runTaskCallback(new Runnable()
				{
					public void run()
					{
						if(getActivity()!=null && mRootView!=null)
						{
							Logcat.d("Fragment.timerRunnable()");
							showContent();
						}
					}
				});
			}
		};
		timerHandler.postDelayed(timerRunnable, delay);
	}
	
	
	private void showProgress()
	{
		// show progress container
		ViewGroup containerContent = (ViewGroup) mRootView.findViewById(R.id.container_content);
		ViewGroup containerProgress = (ViewGroup) mRootView.findViewById(R.id.container_progress);
		ViewGroup containerOffline = (ViewGroup) mRootView.findViewById(R.id.container_offline);
		ViewGroup containerEmpty = (ViewGroup) mRootView.findViewById(R.id.container_empty);
		containerContent.setVisibility(View.GONE);
		containerProgress.setVisibility(View.VISIBLE);
		containerOffline.setVisibility(View.GONE);
		containerEmpty.setVisibility(View.GONE);
		mViewState = ViewState.PROGRESS;
	}
	
	
	private void showOffline()
	{
		// show offline container
		ViewGroup containerContent = (ViewGroup) mRootView.findViewById(R.id.container_content);
		ViewGroup containerProgress = (ViewGroup) mRootView.findViewById(R.id.container_progress);
		ViewGroup containerOffline = (ViewGroup) mRootView.findViewById(R.id.container_offline);
		ViewGroup containerEmpty = (ViewGroup) mRootView.findViewById(R.id.container_empty);
		containerContent.setVisibility(View.GONE);
		containerProgress.setVisibility(View.GONE);
		containerOffline.setVisibility(View.VISIBLE);
		containerEmpty.setVisibility(View.GONE);
		mViewState = ViewState.OFFLINE;
	}
	
	
	private void showEmpty()
	{
		// show empty container
		ViewGroup containerContent = (ViewGroup) mRootView.findViewById(R.id.container_content);
		ViewGroup containerProgress = (ViewGroup) mRootView.findViewById(R.id.container_progress);
		ViewGroup containerOffline = (ViewGroup) mRootView.findViewById(R.id.container_offline);
		ViewGroup containerEmpty = (ViewGroup) mRootView.findViewById(R.id.container_empty);
		containerContent.setVisibility(View.GONE);
		containerProgress.setVisibility(View.GONE);
		containerOffline.setVisibility(View.GONE);
		containerEmpty.setVisibility(View.VISIBLE);
		mViewState = ViewState.EMPTY;
	}
	
	
	private void renderView()
	{
		// reference
		final WebView webView = (WebView) mRootView.findViewById(R.id.fragment_main_webview);
		//final AdView adView = (AdView) mRootView.findViewById(R.id.fragment_main_adview);

		// webview settings
		webView.getSettings().setBuiltInZoomControls(false);
		webView.getSettings().setSupportZoom(true);
        webView.getSettings().setAppCacheEnabled(true);
        webView.getSettings().setAppCachePath(getActivity().getCacheDir().getAbsolutePath());
        webView.getSettings().setCacheMode(WebSettings.LOAD_DEFAULT);
		webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setUserAgentString(getString(R.string.http_user_client));
		webView.setBackgroundColor(getResources().getColor(R.color.global_bg_front));
		webView.setScrollBarStyle(WebView.SCROLLBARS_OUTSIDE_OVERLAY); // fixes scrollbar on Froyo
		webView.setWebChromeClient(new WebChromeClient()
		{
			public void openFileChooser(ValueCallback<Uri> filePathCallback)
			{
				mFilePathCallback4 = filePathCallback;
				Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
				intent.addCategory(Intent.CATEGORY_OPENABLE);
				intent.setType("*/*");
				startActivityForResult(Intent.createChooser(intent, "File Chooser"), REQUEST_FILE_PICKER);
			}


			public void openFileChooser(ValueCallback filePathCallback, String acceptType)
			{
				mFilePathCallback4 = filePathCallback;
				Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
				intent.addCategory(Intent.CATEGORY_OPENABLE);
				intent.setType("*/*");
				startActivityForResult(Intent.createChooser(intent, "File Chooser"), REQUEST_FILE_PICKER);
			}


			public void openFileChooser(ValueCallback<Uri> filePathCallback, String acceptType, String capture)
			{
				mFilePathCallback4 = filePathCallback;
				Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
				intent.addCategory(Intent.CATEGORY_OPENABLE);
				intent.setType("*/*");
				startActivityForResult(Intent.createChooser(intent, "File Chooser"), REQUEST_FILE_PICKER);
			}


			@Override
			public boolean onShowFileChooser(WebView webView, ValueCallback<Uri[]> filePathCallback, WebChromeClient.FileChooserParams fileChooserParams)
			{
				mFilePathCallback5 = filePathCallback;
				Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
				intent.addCategory(Intent.CATEGORY_OPENABLE);
				intent.setType("*/*");
				startActivityForResult(Intent.createChooser(intent, "File Chooser"), REQUEST_FILE_PICKER);
				return true;
			}


            @Override public void onGeolocationPermissionsShowPrompt(String origin, GeolocationPermissions.Callback callback)
            {
                callback.invoke(origin, true, false);
            }
		});
		webView.setWebViewClient(new WebViewClient()
		{
            boolean timeout = true;
            boolean timedout = false;

            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                    try {
                        Thread.sleep(getResources().getInteger(R.integer.server_webview_timeout));
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    if(timeout) {
                        timedout = true;
                        runTaskCallback(new Runnable()
                        {
                            public void run()
                            {
                            if(getActivity()!=null)
                            {
                                showEmpty();
                                showActionBarProgress(false);
                            }
                            }
                        });
                    }
                    }
                }).start();
            }

			@Override
			public void onPageFinished(final WebView view, final String url)
			{
                if (!timedout) {
                    timeout = false;
                    runTaskCallback(new Runnable() {
                        public void run() {
                        if (getActivity() != null) {
                            showContent(500); // hide progress bar with delay to show webview content smoothly
                            showActionBarProgress(false);
                        }
                        }
                    });
                }
			}


			@Override
			public void onReceivedError(final WebView view, final int errorCode, final String description, final String failingUrl)
			{
				runTaskCallback(new Runnable()
				{
					public void run()
					{
                    if(getActivity()!=null)
                    {
                        showEmpty();
                        showActionBarProgress(false);
                    }
					}
				});
			}


			@Override
			public boolean shouldOverrideUrlLoading(WebView view, String url)
			{
				if(DownloadUtility.isDownloadableFile(url))
				{
					Context context = getActivity();
					DisplayToast.createToast(context, context.getString(R.string.WebViewDownloadingToast), Toast.LENGTH_LONG, 1);
					DownloadUtility.downloadFile(getActivity(), url, DownloadUtility.getFileName(url));
					return true;
				}
				else if(url!=null && (url.startsWith("http://") || url.startsWith("https://")))
				{
					// determine for opening the link externally or internally
					boolean external = isLinkExternal(url);
					boolean internal = isLinkInternal(url);
					if(!external && !internal)
					{
						external = WebViewAppConfig.OPEN_LINKS_IN_EXTERNAL_BROWSER;
					}

					// open the link
					if(external)
					{
						startWebActivity(url);
						return true;
					}
					else
					{
						showActionBarProgress(true);
						return false;
					}
				}
				else if(url!=null && url.startsWith("mailto:"))
				{
					MailTo mailTo = MailTo.parse(url);
					startEmailActivity(mailTo.getTo(), mailTo.getSubject(), mailTo.getBody());
					return true;
				}
				else if(url!=null && url.startsWith("tel:"))
				{
					startCallActivity(url);
					return true;
				}
				else if(url!=null && url.startsWith("sms:"))
				{
					startSmsActivity(url);
					return true;
				}
				else if(url!=null && url.startsWith("geo:"))
				{
					startMapSearchActivity(url);
					return true;
				}
				else
				{
					return false;
				}
			}
		});
		webView.setOnKeyListener(new View.OnKeyListener()
		{
			@Override
			public boolean onKey(View v, int keyCode, KeyEvent event)
			{
            if(event.getAction() == KeyEvent.ACTION_DOWN)
            {
                WebView webView = (WebView) v;

                switch(keyCode)
                {
                    case KeyEvent.KEYCODE_BACK:
                        if(webView.canGoBack())
                        {
                            webView.goBack();
                            return true;
                        }
                        break;
                }
            }

            return false;
			}
		});
		webView.requestFocus(View.FOCUS_DOWN);
		webView.setOnTouchListener(new View.OnTouchListener()
		{
			@Override
			public boolean onTouch(View v, MotionEvent event)
			{
            switch(event.getAction())
            {
                case MotionEvent.ACTION_DOWN:
                case MotionEvent.ACTION_UP:
                    if(!v.hasFocus())
                    {
                        v.requestFocus();
                    }
                    break;
            }

            return false;
			}
		});

	}


	private void controlBack()
	{
		final WebView webView = (WebView) mRootView.findViewById(R.id.fragment_main_webview);
		if(webView.canGoBack()) webView.goBack();
	}


	private void controlForward()
	{
		final WebView webView = (WebView) mRootView.findViewById(R.id.fragment_main_webview);
		if(webView.canGoForward()) webView.goForward();
	}


	private void controlStop()
	{
		final WebView webView = (WebView) mRootView.findViewById(R.id.fragment_main_webview);
		webView.stopLoading();
	}


	private void controlReload()
	{
		final WebView webView = (WebView) mRootView.findViewById(R.id.fragment_main_webview);
		webView.reload();
	}


	private void startWebActivity(String url)
	{
		try
		{
			Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
			startActivity(intent);
		}
		catch(ActivityNotFoundException e)
		{
			// can't start activity
		}
	}


	private void startEmailActivity(String email, String subject, String text)
	{
		try
		{
			StringBuilder builder = new StringBuilder();
			builder.append("mailto:");
			builder.append(email);

			Intent intent = new Intent(Intent.ACTION_SENDTO, Uri.parse(builder.toString()));
			intent.putExtra(android.content.Intent.EXTRA_SUBJECT, subject);
			intent.putExtra(android.content.Intent.EXTRA_TEXT, text);
			startActivity(intent);
		}
		catch(ActivityNotFoundException e)
		{
			// can't start activity
		}
	}


	private void startCallActivity(String url)
	{
		try
		{
			Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse(url));
			startActivity(intent);
		}
		catch(ActivityNotFoundException e)
		{
			// can't start activity
		}
	}


	private void startSmsActivity(String url)
	{
		try
		{
			Intent intent = new Intent(Intent.ACTION_SENDTO, Uri.parse(url));
			startActivity(intent);
		}
		catch(ActivityNotFoundException e)
		{
			// can't start activity
		}
	}


	private void startMapSearchActivity(String url)
	{
		try
		{
			Intent intent = new Intent(android.content.Intent.ACTION_VIEW, Uri.parse(url));
			startActivity(intent);
		}
		catch(android.content.ActivityNotFoundException e)
		{
			// can't start activity
		}
	}


	private void startShareActivity(String subject, String text)
	{
		try
		{
			Intent intent = new Intent(android.content.Intent.ACTION_SEND);
			intent.setType("text/plain");
			intent.putExtra(android.content.Intent.EXTRA_SUBJECT, subject);
			intent.putExtra(android.content.Intent.EXTRA_TEXT, text);
			startActivity(intent);
		}
		catch(android.content.ActivityNotFoundException e)
		{
			// can't start activity
		}
	}


	private boolean isLinkExternal(String url)
	{
		for(String rule : WebViewAppConfig.LINKS_OPENED_IN_EXTERNAL_BROWSER)
		{
			if(url.contains(rule)) return true;
		}
		return false;
	}


	private boolean isLinkInternal(String url)
	{
		for(String rule : WebViewAppConfig.LINKS_OPENED_IN_INTERNAL_WEBVIEW)
		{
			if(url.contains(rule)) return true;
		}
		return false;
	}
}
