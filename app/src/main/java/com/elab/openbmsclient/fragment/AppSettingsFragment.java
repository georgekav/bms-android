package com.elab.openbmsclient.fragment;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.elab.openbmsclient.R;
import com.elab.openbmsclient.service.SendDynIDService;
import com.elab.openbmsclient.task.TaskFragment;
import com.elab.openbmsclient.utility.display.DisplayToast;

import java.util.ArrayList;
import java.util.List;


public class AppSettingsFragment extends TaskFragment{
    private static final String ARGUMENT_URL = "url";
    private static final String ARGUMENT_SHARE = "share";
    static ArrayAdapter<String> arrayAdapter;
    static List<String> your_array_list = new ArrayList<>();

    private View mRootView;
    Button but_cancel;
    Button but_apply;

    static Context mContext;
    public static String TAG = "";

    Handler mHandler;


    /***************
     * App lifecycle
     * @return fragment
     */
    public static AppSettingsFragment newInstance(){
        AppSettingsFragment fragment = new AppSettingsFragment();

        // arguments
        Bundle arguments = new Bundle();
        fragment.setArguments(arguments);

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        mContext =this.getActivity();
        TAG = mContext.getClass().getSimpleName();

        setHasOptionsMenu(true);
        setRetainInstance(true);


        SharedPreferences settings = mContext.getSharedPreferences(SendDynIDService.GlobalSharedParamName, Context.MODE_PRIVATE);
        Log.i(TAG, "SharedPref: " + settings.getAll());


        mHandler = new Handler();

        SharedPreferences preferences = mContext.getSharedPreferences(SendDynIDService.GlobalSharedParamName, mContext.MODE_PRIVATE);
        preferences.registerOnSharedPreferenceChangeListener(prefListener);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        mRootView = inflater.inflate(R.layout.fragment_appsettings, container, false);
        return mRootView;
    }

    @Override
    public void onResume()
    {
        super.onResume();
        Log.i(TAG, "onResume");

        // Set user id textview
        TextView textView  = (TextView) this.getActivity().findViewById(R.id.textInputIP);
        TextView textView2 = (TextView) this.getActivity().findViewById(R.id.textInputInterval);
        SharedPreferences settings = this.getActivity().getSharedPreferences(SendDynIDService.GlobalSharedParamName, Context.MODE_PRIVATE);
        textView.setText(settings.getString(SendDynIDService.SParamServerHost, getString(R.string.server_host_default)));
        textView2.setText(String.valueOf(settings.getInt(SendDynIDService.SParamLocUpdateInterval, getResources().getInteger(R.integer.location_update_interval))));

        // Receiver to update the user ID textview
        but_cancel = (Button) mRootView.findViewById(R.id.appSetCancel);
        but_apply = (Button) mRootView.findViewById(R.id.appSetApply);

        but_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //restore from shared preferences
                TextView textView = (TextView) AppSettingsFragment.this.getActivity().findViewById(R.id.textInputIP);
                TextView textView2 = (TextView) AppSettingsFragment.this.getActivity().findViewById(R.id.textInputInterval);
                SharedPreferences settings = AppSettingsFragment.this.getActivity().getSharedPreferences(SendDynIDService.GlobalSharedParamName, Context.MODE_PRIVATE);
                textView.setText(settings.getString(SendDynIDService.SParamServerHost, getString(R.string.server_host_default)));
                textView2.setText(String.valueOf(settings.getInt(SendDynIDService.SParamLocUpdateInterval, getResources().getInteger(R.integer.location_update_interval))));
            }
        });
        but_apply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //store to shared preferences
                TextView textView = (TextView) AppSettingsFragment.this.getActivity().findViewById(R.id.textInputIP);
                TextView textView2 = (TextView) AppSettingsFragment.this.getActivity().findViewById(R.id.textInputInterval);

                SharedPreferences settings = AppSettingsFragment.this.getActivity().getSharedPreferences(SendDynIDService.GlobalSharedParamName, Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = settings.edit();
                String server_host = textView.getText().toString();

                try {
                    Integer loc_update_interval = Integer.parseInt(textView2.getText().toString());
                    editor.putString(SendDynIDService.SParamServerHost, server_host);
                    editor.putInt(SendDynIDService.SParamLocUpdateInterval, loc_update_interval);
                    editor.apply();
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    DisplayToast.createToast(mContext, getString(R.string.AppSettingsNeedToRestartToast), Toast.LENGTH_SHORT, 1);

                    AppSettingsFragment.this.getActivity().finish();

                } catch (NumberFormatException e) {
                    Log.i(TAG, "Interval applied by user is not a number");
                    DisplayToast.createToast(mContext, mContext.getString(R.string.IntervalIsNotANumber), Toast.LENGTH_SHORT, 1);
                    textView2.setText(String.valueOf(settings.getInt(SendDynIDService.SParamLocUpdateInterval, getResources().getInteger(R.integer.location_update_interval))));
                }


                //I cannot make it restart...
//                try {
//                    Thread.sleep(1000);
//                } catch (InterruptedException e) {
//                    e.printStackTrace();
//                }
//
//                Intent i = AppSettingsFragment.this.getActivity().getBaseContext().getPackageManager().getLaunchIntentForPackage(
//                        AppSettingsFragment.this.getActivity().getBaseContext().getPackageName() );
//                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//                i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                AppSettingsFragment.this.getActivity().startActivity(i);
//                AppSettingsFragment.this.getActivity().finish();
            }
        });

        Log.i(TAG, "SharedPref onResume: " + settings.getAll());

    }


    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mRootView = null;
    }

    SharedPreferences.OnSharedPreferenceChangeListener prefListener =
            new SharedPreferences.OnSharedPreferenceChangeListener() {
                public void onSharedPreferenceChanged(SharedPreferences prefs, String key) {
                    Log.i(TAG, "SharedPrefListener " + key + ". " + prefs.getAll());
                }
            };
}
