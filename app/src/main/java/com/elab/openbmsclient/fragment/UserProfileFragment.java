package com.elab.openbmsclient.fragment;

import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.widget.SwitchCompat;
import android.text.InputType;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.elab.openbmsclient.R;
import com.elab.openbmsclient.activity.MainActivity;
import com.elab.openbmsclient.receiver.AirplaneModeReceiver;
import com.elab.openbmsclient.service.LocalizationServiceUpdateGeofence;
import com.elab.openbmsclient.service.SendDynIDService;
import com.elab.openbmsclient.task.TaskFragment;
import com.elab.openbmsclient.utility.RuntimePermissions;
import com.elab.openbmsclient.utility.display.CheckLocationDisplayWirelessSettingsDialog;
import com.elab.openbmsclient.utility.display.CheckNFCDisplayWirelessSettingsDialog;
import com.elab.openbmsclient.utility.display.DisplaySetUserIDDialog;
import com.elab.openbmsclient.utility.display.DisplayToast;
import com.elab.openbmsclient.utility.display.DisplayToastRunnable;
import com.elab.openbmsclient.utility.geofences.GeofenceManager;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


public class UserProfileFragment extends TaskFragment{
    private static final String ARGUMENT_URL = "url";
    private static final String ARGUMENT_SHARE = "share";
    static ArrayAdapter<String> arrayAdapter;
    static List<String> your_array_list = new ArrayList<>();
    String item;

    private View mRootView;
    Button but_v;
    ListView list;
    public static SwitchCompat switch1;
    static Button loadFences;
    static Button addFence;
    static Button deleteFence;

    static Context mContext;
    public static String TAG = "";

    private LocationManager locationManager;
    public GeofenceManager mGeofenceManager;
    Intent startServiceIntent;

    // Flag for GPS status
    boolean isGPSEnabled = false;

    // Flag for network status
    boolean isNetworkEnabled = false;

    Handler mHandler;

    String fenceName = "default";
    String fenceRadius = "500";
    String fenceType = "work";

    /***************
     * App lifecycle
     * @return fragment
     */
    public static UserProfileFragment newInstance(){
        UserProfileFragment fragment = new UserProfileFragment();

        // arguments
        Bundle arguments = new Bundle();
        fragment.setArguments(arguments);

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        mContext =this.getActivity();
        TAG = mContext.getClass().getSimpleName();

        setHasOptionsMenu(true);
        setRetainInstance(true);


        SharedPreferences settings = mContext.getSharedPreferences(SendDynIDService.GlobalSharedParamName, Context.MODE_PRIVATE);
        Log.i(TAG, "SharedPref: " + settings.getAll());


        mGeofenceManager = new GeofenceManager(mContext);

        mHandler = new Handler();

        SharedPreferences preferences = mContext.getSharedPreferences(SendDynIDService.GlobalSharedParamName, mContext.MODE_PRIVATE);
        preferences.registerOnSharedPreferenceChangeListener(prefListener);

        startServiceIntent = new Intent(mContext, LocalizationServiceUpdateGeofence.class);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        mRootView = inflater.inflate(R.layout.fragment_userprofile, container, false);
        return mRootView;
    }

    @Override
    public void onResume()
    {
        super.onResume();
        Log.i(TAG, "onResume");

        // Set user id textview
        TextView textView  = (TextView) this.getActivity().findViewById(R.id.TV_CurrentID);
        TextView textView2 = (TextView) this.getActivity().findViewById(R.id.TV_CurrentUser);
        SharedPreferences settings =    this.getActivity().getSharedPreferences(SendDynIDService.GlobalSharedParamName, Context.MODE_PRIVATE);
        textView.setText(settings.getString(SendDynIDService.SParamNameUserID, getString(R.string.DEFAULT_ID)));
        textView2.setText(settings.getString(SendDynIDService.SParamNameUser, getString(R.string.DEFAULT_ID)));

        // Receiver to update the user ID textview
        this.getActivity().registerReceiver(TVUpdater, new IntentFilter(SendDynIDService.IntentNameTextViewUpdater));
        but_v = (Button) mRootView.findViewById(R.id.B_Set_User);
        list = (ListView) this.getActivity().findViewById(R.id.listView);
        switch1 = (SwitchCompat) this.getActivity().findViewById(R.id.S_Activate_Geofences);
        loadFences = (Button) this.getActivity().findViewById(R.id.B_Load_Geofence);
        addFence =(Button) this.getActivity().findViewById(R.id.B_Add_Geofence);
        deleteFence =(Button) this.getActivity().findViewById(R.id.B_Delete_Geofence);


        arrayAdapter = new ArrayAdapter<>(this.getActivity(), android.R.layout.simple_list_item_1, your_array_list);
        list.setAdapter(arrayAdapter);

        updateView();


        but_v.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mHandler.post(new DisplaySetUserIDDialog(UserProfileFragment.this.getActivity()));
            }
        });

        Log.i(TAG, "SharedPref onResume: " + settings.getAll());
        boolean fenceState = settings.getBoolean(GeofenceManager.SParamNameFenceState, false);
        if (fenceState) {
            list.setAlpha(1);
            switch1.setChecked(true);
        } else {
            list.setAlpha((float)0.2);
            switch1.setChecked(false);
        }

        switch1.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                SharedPreferences settings = mContext.getSharedPreferences(SendDynIDService.GlobalSharedParamName, Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = settings.edit();


                if (isChecked) {
                    String UserID = settings.getString(SendDynIDService.SParamNameUserID, getString(R.string.DEFAULT_ID));
                    boolean UserIDValid = !UserID.equals(getString(R.string.DEFAULT_ID)) && !UserID.equals("null");
                    if (!UserIDValid) {
                        //MainActivity.SetUserID();
                        mHandler.post(new DisplayToastRunnable(mContext, getString(R.string.AlertRegisterUserBefore), 0));
                        switch1.setChecked(false);
                        return; //Nothing to do more
                    }
                    if (!RuntimePermissions.location_permission_granted) {
                        AlertDialog.Builder alertDialog = new AlertDialog.Builder(mContext);
                        alertDialog.setTitle("Localization Permission");
                        alertDialog.setMessage(R.string.LocationPermissionDeniedToast);
                        alertDialog.setNegativeButton("Acknowledge", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.cancel();
                                ((MainActivity) mContext).permissions_requester.requestPermissions();
                            }
                        });
                        alertDialog.create().show();

                        switch1.setChecked(false);
                        return; //Nothing to do more
                    }

                    String s = settings.getString(GeofenceManager.SParamNameFenceID, null);
                    Boolean status = false;

                    if (s != null) {
                        status = true;
                        if (s.equals("")) {
                            status = false;
                        }
                    }
                    editor.putBoolean(GeofenceManager.SParamNameFenceState, true);
                    list.setAlpha(1);

                    if (status) {
                        editor.putBoolean(GeofenceManager.SParamNameFenceState, true);
                        mGeofenceManager.registerGeofences();
                        list.setAlpha(1);
                    } else {
                        DisplayToast.createToast(mContext, mContext.getString(R.string.GeofenceSwitchMissingGeofenceToast), Toast.LENGTH_SHORT, 0);
                        editor.putBoolean(GeofenceManager.SParamNameFenceState, false);
                        switch1.setChecked(false);
                        list.setAlpha((float) 0.2);
                    }
                } else {
                    editor.putBoolean(GeofenceManager.SParamNameFenceState, false);
                    list.setAlpha((float) 0.2);

                    mGeofenceManager.unRegisterAllGeofences();
                    Intent stop_update_loc_intent = new Intent(LocalizationServiceUpdateGeofence.IntentNameStopLocationUpdate);
                    mContext.sendBroadcast(stop_update_loc_intent);
                    unregisterLocUpdate();
                }
                editor.apply();
            }
        });

        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> paramAdapterView, View paramView, final int paramInt, long paramLong) {
                item = (String) list.getItemAtPosition(paramInt);
                if (switch1.isChecked()) { //Allow add/remove a fence only of the master switch is active
                    // Open dialog
                    AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
                    builder.setMessage("Delete the " + item + " ?");

                    builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialogInterface, int i) {
                            mGeofenceManager.removeFence(item);
                        }
                    });
                    builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialogInterface, int i) {
                            //pass
                        }
                    });
                    builder.create().show();
                }
            }
        });

        loadFences.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String FENCES_Lat_save   = getString(R.string.FencesLat);
                String FENCES_Lon_save   =getString(R.string.FencesLon);
                String FENCES_ID_save    = getString(R.string.FencesID);
                String FENCES_Reg_save   =getString(R.string.FencesReg);
                String FENCE_Type_save   = getString(R.string.FencesType);
                String FENCE_Radius_save = getString(R.string.FencesRadius);

                SharedPreferences settings = mContext.getSharedPreferences(SendDynIDService.GlobalSharedParamName, Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = settings.edit();

                editor.putString(GeofenceManager.SParamNameFenceLatitude, FENCES_Lat_save);
                editor.putString(GeofenceManager.SParamNameFenceLongitude, FENCES_Lon_save);
                editor.putString(GeofenceManager.SParamNameFenceID, FENCES_ID_save);
                editor.putString(GeofenceManager.SParamNameFenceReg, FENCES_Reg_save);
                editor.putString(GeofenceManager.SParamNameFenceType, FENCE_Type_save);
                editor.putString(GeofenceManager.SParamNameFenceRadius, FENCE_Radius_save);
                editor.putString(AirplaneModeReceiver.SParamNameAirplaneMode, "OFF");
                editor.apply();


                if(switch1.isChecked()) {
                    mGeofenceManager.registerGeofences();
                }
                updateView();
            }
        });

        addFence.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!RuntimePermissions.location_permission_granted) {
                    //MainActivity.SetUserID();
                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(mContext);
                    alertDialog.setTitle("Localization Permission");
                    alertDialog.setMessage(R.string.LocationPermissionDeniedToast);
                    alertDialog.setNegativeButton("Acknowledge", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();
                            ((MainActivity) mContext).permissions_requester.requestPermissions();
                        }
                    });
                    alertDialog.create().show();
                    return; //Nothing to do more
                }
                locationManager = (LocationManager) mContext.getSystemService(Context.LOCATION_SERVICE);
                // Getting GPS/ network status
                isGPSEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
                isNetworkEnabled = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);

                AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
                builder.setMessage("Add a new fence here");

                LayoutInflater li = LayoutInflater.from(mContext);
                final View dialogView = li.inflate(R.layout.add_fence, null);

                final Spinner spinnerType = (Spinner) dialogView.findViewById(R.id.spinnerType);
                ArrayAdapter<CharSequence> adapterType = ArrayAdapter.createFromResource(mContext, R.array.type_vect, android.R.layout.simple_spinner_item);
                adapterType.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                spinnerType.setAdapter(adapterType);

                spinnerType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
                        fenceType = spinnerType.getItemAtPosition(arg2).toString();
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> arg0) {
                        // TODO Auto-generated method stub
                    }
                });

                final Spinner spinnerRadius = (Spinner) dialogView.findViewById(R.id.spinnerRadius);
                ArrayAdapter<CharSequence> adapterRadius = ArrayAdapter.createFromResource(mContext, R.array.radius_vect, android.R.layout.simple_spinner_item);
                adapterRadius.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                spinnerRadius.setAdapter(adapterRadius);

                spinnerRadius.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
                        fenceRadius = spinnerRadius.getItemAtPosition(arg2).toString();
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> arg0) {
                        // Auto-generated method stub
                    }
                });

                final EditText input = (EditText) dialogView.findViewById(R.id.edittextDialog);

                //final EditText edittext= new EditText(mContext);
                input.setHint("default name");
                input.setInputType(InputType.TYPE_TEXT_FLAG_NO_SUGGESTIONS);

                builder.setView(dialogView);

                builder.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialogInterface, int i) {
                        fenceName = input.getText().toString();

                        if (nameOK(fenceName)) {
                            addFence.setEnabled(false);
                            addFence.setText(mContext.getString(R.string.UserProfileGeoAddButtonWorking));
                            registerLocUpdate();
                            startServiceIntent.putExtra(LocalizationServiceUpdateGeofence.ExtraNameRequestType, "Single");
                            startServiceIntent.putExtra(LocalizationServiceUpdateGeofence.ExtraRequestFenceRadius, fenceRadius);
                            mContext.startService(startServiceIntent);
                        } else {
                            DisplayToast.createToast(mContext, getString(R.string.UserProfileWrongFenceNameToast), Toast.LENGTH_SHORT, 0);
                        }
                    }
                });
                builder.setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialogInterface, int i) {
                        //mContext.finish();
                    }
                });
                builder.create().show();
            }
        });

        deleteFence.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Open dialog
                AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
                builder.setMessage(getString(R.string.UserProfileDeleteAllFence));
                builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialogInterface, int i) {

                        mGeofenceManager.removeFence();
                    }
                });
                builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialogInterface, int i) {
                        //pass
                    }
                });
                builder.create().show();
            }
        });

        // Check and invite user to activate NFC and localisation
        mHandler.post(new CheckLocationDisplayWirelessSettingsDialog(this.getActivity()));
        mHandler.post(new CheckNFCDisplayWirelessSettingsDialog(this.getActivity()));
    }

    boolean nameOK(String str) {
        boolean valid = false;
        if (str == null) {
            valid = false;
        } else if (str.equals("")) {
            valid = false;
        } else if (str.contains(" ")) {
            valid = false;
        } else if (str.matches("[a-zA-Z0-9.? ]*")) {
            valid = true;
        }

        return valid;
    }

    @Override
    public void onPause() {
        super.onPause();
        // Unregister Receiver
        this.getActivity().unregisterReceiver(TVUpdater);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        mGeofenceManager.disconnectAPI();

        mRootView = null;
    }

    // Location management
    private void registerLocUpdate() {
        Log.i(TAG, "registerLocUpdate");

        // Receiver to update the user ID textview
        mContext.registerReceiver(LocUpdater, new IntentFilter(LocalizationServiceUpdateGeofence.IntentNameSingleLocationUpdate));
    }

    private void unregisterLocUpdate() {
        Log.i(TAG, "unregisterLocUpdate");

        // Unregister Receiver
        try {mContext.unregisterReceiver(LocUpdater);
        }
        catch(IllegalArgumentException e)
        {//Not registered
        }

    }

    // Receiver to update the localisation in order to create the correct fence
    private BroadcastReceiver LocUpdater = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Log.i(TAG, "BroadcastReceiver LocUpdater onReceive");

            double latitude = intent.getExtras().getDouble(LocalizationServiceUpdateGeofence.ExtraNameLatitude);
            double longitude = intent.getExtras().getDouble(LocalizationServiceUpdateGeofence.ExtraNameLongitude);
            double acc = intent.getExtras().getDouble(LocalizationServiceUpdateGeofence.ExtraNameAccuracy);

            if (latitude != 0.0) {
                mGeofenceManager.addFence(latitude, longitude, acc, fenceName, fenceType, fenceRadius);
                unregisterLocUpdate();
            }
        }
    };

    public static void updateView() {
        Log.i(TAG, "updateView");
        if (mContext != null) {
            SharedPreferences settings = mContext.getSharedPreferences(SendDynIDService.GlobalSharedParamName, Context.MODE_PRIVATE);
            List<String> separatedFences = new ArrayList<>(Arrays.asList(settings.getString(GeofenceManager.SParamNameFenceID, "").split(":")));
            List<String> separatedRadiuss = new ArrayList<>(Arrays.asList(settings.getString(GeofenceManager.SParamNameFenceRadius, "").split(":")));
            List<String> separatedTypes = new ArrayList<>(Arrays.asList(settings.getString(GeofenceManager.SParamNameFenceType, "").split(":")));

            your_array_list.clear();
            if (separatedFences != null) {
                if (!separatedFences.get(0).equals("")) {
                    for (int i = 0; i < separatedFences.size(); i++) {
                        String separatedFence = separatedFences.get(i);
                        String separatedRadius = separatedRadiuss.get(i);
                        String separatedType = separatedTypes.get(i);
                        your_array_list.add(0, separatedFence + " - " + separatedType + " - " + separatedRadius);
                    }
                    addFence.setEnabled(true);
                    addFence.setText(mContext.getString(R.string.UserProfileGeoAddButton));
                }
            }
            arrayAdapter.notifyDataSetChanged();
        }
    }


    // Receiver to update the TextView when the user ID is updated
    private BroadcastReceiver TVUpdater = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            // Set user id TextView
            TextView textView = (TextView) UserProfileFragment.this.getActivity().findViewById(R.id.TV_CurrentID);
            textView.setText(intent.getExtras().getString(SendDynIDService.SParamNameUserID));
            TextView textView2 = (TextView) UserProfileFragment.this.getActivity().findViewById(R.id.TV_CurrentUser);
            textView2.setText(intent.getExtras().getString(SendDynIDService.SParamNameUser));
        }
    };

    SharedPreferences.OnSharedPreferenceChangeListener prefListener =
            new SharedPreferences.OnSharedPreferenceChangeListener() {
                public void onSharedPreferenceChanged(SharedPreferences prefs, String key) {
                    Log.i(TAG, "SharedPrefListener " + key + ". " + prefs.getAll());
                }
            };
}
