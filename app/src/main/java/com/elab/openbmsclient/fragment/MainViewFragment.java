package com.elab.openbmsclient.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.elab.openbmsclient.R;
import com.elab.openbmsclient.task.TaskFragment;

/**
 * Created by lilis on 13.07.2015.
 */
public class MainViewFragment extends TaskFragment{
    private View mRootView;

    public static MainViewFragment newInstance(){
        MainViewFragment fragment = new MainViewFragment();

        // arguments
        Bundle arguments = new Bundle();
        fragment.setArguments(arguments);

        return fragment;
    }
    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        mRootView = inflater.inflate(R.layout.fragment_main, container, false);
        return mRootView;
    }
}


