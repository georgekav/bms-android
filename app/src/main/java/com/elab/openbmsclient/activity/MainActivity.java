package com.elab.openbmsclient.activity;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.Toast;

import com.elab.openbmsclient.R;
import com.elab.openbmsclient.adapter.DrawerAdapter;
import com.elab.openbmsclient.fragment.AppSettingsFragment;
import com.elab.openbmsclient.fragment.MainViewFragment;
import com.elab.openbmsclient.fragment.UserProfileFragment;
import com.elab.openbmsclient.fragment.WebViewFragment;
import com.elab.openbmsclient.service.SendDynIDService;
import com.elab.openbmsclient.utility.RuntimePermissions;
import com.elab.openbmsclient.utility.display.DisplayToast;


public class MainActivity extends ActionBarActivity
{
    private DrawerLayout            mDrawerLayout;
    private ActionBarDrawerToggle   mDrawerToggle;
    private ListView                mDrawerListView;
    private CharSequence            mTitle;
    private CharSequence            mDrawerTitle;
    private String[]                mTitles;
    private String                  mServerHost;
    private String[]                mUrlList;
    private String[]                mShareList;
    private Fragment[]              mFragments;
    private int                     prev_selection;
    public final RuntimePermissions permissions_requester = new RuntimePermissions(this);
    private static final String TAG = MainActivity.class.getSimpleName();


    public static Intent newIntent(Context context)
    {
        Intent intent = new Intent(context, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        return intent;
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);
        setupActionBar();
        //Android M permission requests
        permissions_requester.requestPermissions();
        //requestPhonePermission();
        //requestStoragePermission();
        setupDrawer(savedInstanceState);
        setupFragments(savedInstanceState);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        // open or close the drawer if home button is pressed
        if(mDrawerToggle.onOptionsItemSelected(item)){
            return true;
        }

        // action bar menu behaviour
        switch(item.getItemId()){
            case android.R.id.home:
                Intent intent = MainActivity.newIntent(this);
                startActivity(intent);
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState){
        super.onPostCreate(savedInstanceState);
        mDrawerToggle.syncState();
    }

    @Override
    public void onSaveInstanceState(Bundle outState){
        super.onSaveInstanceState(outState);
        FragmentManager fragmentManager = getSupportFragmentManager();
        //Save all fragments references
        for(int i = 0; i < mFragments.length; i++) {
            fragmentManager.putFragment(outState, "frag_"+Integer.toString(i), mFragments[i]);
        }
        //Save the currently selected fragment
        outState.putInt("prev_selection", prev_selection);
    }


    @Override
    public void onConfigurationChanged(Configuration newConfiguration){
        super.onConfigurationChanged(newConfiguration);
        mDrawerToggle.onConfigurationChanged(newConfiguration);
    }


    @Override
    public void setTitle(CharSequence title){
        mTitle = title;
        getSupportActionBar().setTitle(mTitle);
    }


    @Override
    public boolean onKeyDown(int keyCode, KeyEvent e){
        if (keyCode == KeyEvent.KEYCODE_MENU){
            if (!mDrawerLayout.isDrawerOpen(mDrawerListView)){
                mDrawerLayout.openDrawer(mDrawerListView);
            }
            else{
                mDrawerLayout.closeDrawer(mDrawerListView);
            }
            return true;
        }
        return super.onKeyDown(keyCode, e);
    }


    private void setupActionBar(){
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        ActionBar bar = getSupportActionBar();
        bar.setDisplayUseLogoEnabled(false);
        bar.setDisplayShowTitleEnabled(true);
        bar.setDisplayShowHomeEnabled(true);
        bar.setDisplayHomeAsUpEnabled(true);
        bar.setHomeButtonEnabled(true);
    }


    @Override
    public void onRequestPermissionsResult(int requestCode,String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case RuntimePermissions.request_permission_id_callback: {
                if (grantResults.length > 0){
                    for(int i=0; i<permissions.length; i++){
                        switch (permissions[i]){
                            case Manifest.permission.ACCESS_FINE_LOCATION:
                                if(grantResults[i] == PackageManager.PERMISSION_GRANTED){
                                    // permission was granted
                                    DisplayToast.createToast(this, getResources().getString(R.string.LocationPermissionGrantedToast), Toast.LENGTH_LONG, 1);
                                    RuntimePermissions.location_permission_granted = true;
                                }
                                else {
                                    // permission denied
                                    DisplayToast.createToast(this, getResources().getString(R.string.LocationPermissionDeniedToast), Toast.LENGTH_LONG, 0);
                                }
                                break;
                            case Manifest.permission.READ_PHONE_STATE:
                                if (grantResults[i] == PackageManager.PERMISSION_GRANTED) {
                                    // permission was granted
                                    DisplayToast.createToast(this, getResources().getString(R.string.PhonePermissionGrantedToast), Toast.LENGTH_LONG, 1);
                                    RuntimePermissions.phone_permission_granted = true;
                                } else {
                                    // permission denied
                                    DisplayToast.createToast(this, getResources().getString(R.string.PhonePermissionDeniedToast), Toast.LENGTH_LONG, 0);
                                }
                                break;
                            case Manifest.permission.WRITE_EXTERNAL_STORAGE:
                                if (grantResults[i] == PackageManager.PERMISSION_GRANTED) {
                                    // permission was granted
                                    DisplayToast.createToast(this, getResources().getString(R.string.StoragePermissionGrantedToast), Toast.LENGTH_LONG, 1);
                                    RuntimePermissions.storage_permission_granted = true;
                                } else {
                                    // permission denied
                                    DisplayToast.createToast(this, getResources().getString(R.string.StoragePermissionDeniedToast), Toast.LENGTH_LONG, 0);
                                }
                                break;
                        }
                    }
                }
                else {
                    DisplayToast.createToast(this, getResources().getString(R.string.AllPermissionsDeniedToast), Toast.LENGTH_LONG, 0);
                }
            }
        }
    }


    private void setupDrawer(Bundle savedInstanceState){
        mTitle = getTitle();
        mDrawerTitle = getTitle();

        // title list
        mTitles = getResources().getStringArray(R.array.navigation_title_list);

        // icon list
        TypedArray iconTypedArray = getResources().obtainTypedArray(R.array.navigation_icon_list);
        Integer[] icons = new Integer[iconTypedArray.length()];
        for(int i=0; i<iconTypedArray.length(); i++){
            icons[i] = iconTypedArray.getResourceId(i, -1);
        }
        iconTypedArray.recycle();

        // reference
        mDrawerLayout = (DrawerLayout) findViewById(R.id.activity_main_layout);
        mDrawerListView = (ListView) findViewById(R.id.activity_main_drawer);

        // set drawer
        mDrawerLayout.setDrawerShadow(R.drawable.drawer_shadow, GravityCompat.START);
        mDrawerListView.setAdapter(new DrawerAdapter(this, mTitles, icons));
        mDrawerListView.setOnItemClickListener(new OnItemClickListener()
        {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View clickedView, int position, long id)
            {
                selectDrawerItem(position, false);
            }
        });
        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, R.string.drawer_open, R.string.drawer_close){
            @Override
            public void onDrawerClosed(View view){
                getSupportActionBar().setTitle(mTitle);
                supportInvalidateOptionsMenu();
            }

            @Override
            public void onDrawerOpened(View drawerView){
                getSupportActionBar().setTitle(mDrawerTitle);
                supportInvalidateOptionsMenu();
            }
        };
        mDrawerLayout.setDrawerListener(mDrawerToggle);
    }


    private void setupFragments(Bundle savedInstanceState){

        SharedPreferences settings = getSharedPreferences(SendDynIDService.GlobalSharedParamName, MODE_PRIVATE);

        //Initialize the parameters
        mServerHost =  settings.getString(SendDynIDService.SParamServerHost, getString(R.string.server_host_default)); //Get the address from store
        mUrlList = getResources().getStringArray(R.array.navigation_url_list);
        mShareList = getResources().getStringArray(R.array.navigation_share_list);
        mFragments = new Fragment[mUrlList.length];
        final int default_pos = getResources().getInteger(R.integer.default_pos_drawer);

        // show initial fragment or restore state
        if(savedInstanceState == null){
            try {
                String current_room = settings.getString(SendDynIDService.SParamUserCurrentRoom, getString(R.string.default_roomid));
                FragmentManager fragmentManager = getSupportFragmentManager();
                for (int i = 0; i < mUrlList.length; i++) {
                    switch (mUrlList[i]){
                        case "MAIN_VIEW":
                            mFragments[i] = MainViewFragment.newInstance();
                            fragmentManager.beginTransaction().add(R.id.activity_main_container, mFragments[i]).commitAllowingStateLoss();
                            fragmentManager.beginTransaction().hide(mFragments[i]).commitAllowingStateLoss();
                            break;
                        case "PER_UUID_SETUP":
                            mFragments[i] = UserProfileFragment.newInstance();
                            fragmentManager.beginTransaction().add(R.id.activity_main_container, mFragments[i]).commitAllowingStateLoss();
                            fragmentManager.beginTransaction().hide(mFragments[i]).commitAllowingStateLoss();
                        break;
                        case "APP_SETTINGS":
                            mFragments[i] = AppSettingsFragment.newInstance();
                            fragmentManager.beginTransaction().add(R.id.activity_main_container, mFragments[i]).commitAllowingStateLoss();
                            fragmentManager.beginTransaction().hide(mFragments[i]).commitAllowingStateLoss();
                            break;
                        default: //aka WebView
                            int pos_with_roomid = getResources().getInteger(R.integer.pos_with_roomid);
                            boolean isaccount_webview = false;
                            if (getResources().getInteger(R.integer.pos_with_accountpage) == i){
                                isaccount_webview = true;
                            }
                            if (pos_with_roomid == i){//Check if this url depends on roomid do that mainfragment registers a listener
                                mFragments[i] = WebViewFragment.newInstance(
                                        String.format(mUrlList[i], mServerHost, current_room),
                                        String.format(mShareList[i], mServerHost, current_room),
                                        true, isaccount_webview);
                            }
                            else{
                                mFragments[i] = WebViewFragment.newInstance(
                                        String.format(mUrlList[i], mServerHost),
                                        String.format(mShareList[i], mServerHost),
                                        false, isaccount_webview);
                            }
                            fragmentManager.beginTransaction().add(R.id.activity_main_container, mFragments[i]).commitAllowingStateLoss();
                            fragmentManager.beginTransaction().hide(mFragments[i]).commitAllowingStateLoss();
                            break;
                    }

                }
            }catch (Error error){
                Log.e(TAG, "setupFragments - error");
            }
            selectDrawerItem(default_pos, false);
        }
        else{
            FragmentManager fragmentManager = getSupportFragmentManager();
            for(int i = 0; i < mUrlList.length; i++) {
                mFragments[i] = fragmentManager.getFragment(savedInstanceState, "frag_"+Integer.toString(i));
                //Hide all because by default android shows all
                fragmentManager.beginTransaction().hide(mFragments[i]).commitAllowingStateLoss();
            }
            prev_selection = savedInstanceState.getInt("prev_selection");
            //Un-hide the previously selected one
            fragmentManager.beginTransaction().show(mFragments[prev_selection]).commitAllowingStateLoss();
            if (prev_selection != default_pos){ //If it is not the default(which used app title) restore toolbar title as well.
                setTitle(mTitles[prev_selection]);
            }
        }

    }

    private void selectDrawerItem(int position, boolean init)
    {
        FragmentManager fragmentManager = getSupportFragmentManager();
        if(init){
            SharedPreferences settings = getSharedPreferences(SendDynIDService.GlobalSharedParamName, MODE_PRIVATE);
            String current_room = settings.getString(SendDynIDService.SParamUserCurrentRoom, getString(R.string.default_roomid));
            int pos_with_roomid = getResources().getInteger(R.integer.pos_with_roomid);
            boolean isaccount_webview = false;
            if (getResources().getInteger(R.integer.pos_with_accountpage) == position){
                isaccount_webview = true;
            }
            if (pos_with_roomid == position){//Check if this url depends on roomid so that mainfragment registers a listener
                mFragments[position] = WebViewFragment.newInstance(
                        String.format(mUrlList[position], mServerHost, current_room),
                        String.format(mShareList[position], mServerHost, current_room),
                        true, isaccount_webview);
            }
            else{
                mFragments[position] = WebViewFragment.newInstance(
                        String.format(mUrlList[position], mServerHost),
                        String.format(mShareList[position], mServerHost),
                        false, isaccount_webview);
            }

            fragmentManager.beginTransaction().add(R.id.activity_main_container, mFragments[position]).commitAllowingStateLoss(); //shows automatically
        }
        else{
            fragmentManager.beginTransaction().hide(mFragments[prev_selection]).commitAllowingStateLoss();

            fragmentManager.beginTransaction().show(mFragments[position]).commitAllowingStateLoss();
            setTitle(mTitles[position]);
        }
        prev_selection = position;

        mDrawerListView.setItemChecked(position, true);
        mDrawerLayout.closeDrawer(mDrawerListView);
    }
}