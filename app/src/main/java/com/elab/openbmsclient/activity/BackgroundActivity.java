package com.elab.openbmsclient.activity;

import android.app.Activity;
import android.app.PendingIntent;
import android.content.Intent;
import android.nfc.NfcAdapter;
import android.os.Bundle;
import android.widget.Toast;

import com.elab.openbmsclient.R;
import com.elab.openbmsclient.service.SendDynIDService;
import com.elab.openbmsclient.utility.display.DisplayToast;

/**
 * By adrienhoffet on 23.03.15.
 */
public class BackgroundActivity extends Activity {
    private PendingIntent mPendingIntent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Manage the intent
        resolveIntent(getIntent());
    }

    public void resolveIntent(Intent intent) {

        String action = intent.getAction();
        if (NfcAdapter.ACTION_TAG_DISCOVERED.equals(action)
                || NfcAdapter.ACTION_TECH_DISCOVERED.equals(action)
                || NfcAdapter.ACTION_NDEF_DISCOVERED.equals(action)) {

            // Retrieve ID from the NFC intent
            byte[] ID = intent.getByteArrayExtra(NfcAdapter.EXTRA_ID);
            String str_id = SendDynIDService.getHex_inv(ID);

            // Create an intent service to treat the NFC tag
            Intent myIntentService = new Intent(getApplicationContext(), SendDynIDService.class);
            myIntentService.putExtra(SendDynIDService.ExtraNameID, str_id);

            // Control if it is a tag or user ID
            if (SendDynIDService.NextIsID) {
                DisplayToast.createToast(this, this.getString(R.string.IDChangeRequestToast), Toast.LENGTH_LONG, 3);
            }
            // Call the Intent Service to process the tag
            startService(myIntentService);
        }
        // Close the activity for no foreground visibility
        finish();
    }
}