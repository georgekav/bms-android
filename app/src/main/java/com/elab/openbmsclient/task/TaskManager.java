package com.elab.openbmsclient.task;


public interface TaskManager
{
	public void runTaskCallback(Runnable runnable);
}
