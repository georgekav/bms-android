package com.elab.openbmsclient.receiver;

import android.annotation.TargetApi;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Handler;
import android.provider.Settings;
import android.widget.Toast;

import com.elab.openbmsclient.R;
import com.elab.openbmsclient.service.SendDynIDService;
import com.elab.openbmsclient.utility.display.DisplayToast;

/**
 *  adrienhoffet on 23.03.15.
 */

public class AirplaneModeReceiver extends BroadcastReceiver {
    public static final String SParamNameAirplaneMode = "airplane_mode";

    @Override
    public void onReceive(final Context context, Intent intent) {
        if(intent.getBooleanExtra("state", false)){
            updateAirplaneMode(context);
        }else{
            Handler h = new Handler();
            Runnable r1 = new Runnable() {
                @Override
                public void run() {
                    updateAirplaneMode(context);
                }
            };
            DisplayToast.createToast(context, context.getString(R.string.AirplaneModeTriggerToast), Toast.LENGTH_SHORT, 3);
            h.postDelayed(r1, 15000); // 15 second delay
        }
    }

    @SuppressWarnings("deprecation")
    @TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    public static boolean isAirplaneModeOn(Context context) {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN_MR1) {
            return Settings.System.getInt(context.getContentResolver(),
                    Settings.System.AIRPLANE_MODE_ON, 0) != 0;
        } else {
            return Settings.Global.getInt(context.getContentResolver(),
                    Settings.Global.AIRPLANE_MODE_ON, 0) != 0;
        }
    }

    public static void updateAirplaneMode(Context context){
        SharedPreferences mSharePref = context.getSharedPreferences(SendDynIDService.GlobalSharedParamName, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = mSharePref.edit();

        if(isAirplaneModeOn(context)) {
            editor.putString(SParamNameAirplaneMode, "ON");
            DisplayToast.createToast(context, context.getString(R.string.AirplaneModeNotifDisabledToast), Toast.LENGTH_SHORT, 3);
        }else{
            editor.putString(SParamNameAirplaneMode, "OFF");
            DisplayToast.createToast(context, context.getString(R.string.AirplaneModeNotifEnabledToast), Toast.LENGTH_SHORT, 3);
        }
        editor.apply();
    }
}