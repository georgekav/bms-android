package com.elab.openbmsclient.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.elab.openbmsclient.R;
import com.elab.openbmsclient.service.BootCompleteHelperService;
import com.elab.openbmsclient.utility.display.DisplayNotification;

/**
 * adrienhoffet on 28.05.15.
 */
public class BootCompleteReceiver extends BroadcastReceiver {
    Intent startServiceIntent;

    @Override
    public void onReceive(final Context context, Intent intent) {
        startServiceIntent = new Intent(context, BootCompleteHelperService.class);
        DisplayNotification.createNotification(context, "Boot Event", context.getString(R.string.BootCompleteNotification), 3);
        context.startService(startServiceIntent);

    }

}
