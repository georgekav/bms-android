package com.elab.openbmsclient.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.elab.openbmsclient.R;
import com.elab.openbmsclient.service.SendDynIDService;

/**
 *  adrienhoffet on 23.03.15.
 */

public class ConnectivityReceiver extends BroadcastReceiver {
    public static boolean SendAll = false;

    @Override
    public void onReceive(Context context, Intent intent) {

        // The if acts like a bounce killer (broadcast received more than once by transition)
        if (!SendAll && haveNetworkConnection(context)){
            SendAll = true;
            Intent myService = new Intent(context, SendDynIDService.class);
            myService.putExtra(SendDynIDService.ExtraNameID, context.getString(R.string.DEFAULT_ID));
            context.startService(myService);
        }
    }

    // Check if the internet connection is valid
    private boolean haveNetworkConnection(Context context) {
        boolean haveConnectedWifi = false;
        boolean haveConnectedMobile = false;

        ConnectivityManager cm = (ConnectivityManager)   context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo[] netInfo = cm.getAllNetworkInfo();
        for (NetworkInfo ni : netInfo) {
            if (ni.getTypeName().equalsIgnoreCase("WIFI"))
                if (ni.isConnected())
                    haveConnectedWifi = true;
            if (ni.getTypeName().equalsIgnoreCase("MOBILE"))
                if (ni.isConnected())
                    haveConnectedMobile = true;
        }
        return haveConnectedWifi || haveConnectedMobile;
    }
}