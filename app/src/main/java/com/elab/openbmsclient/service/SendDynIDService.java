package com.elab.openbmsclient.service;

import android.app.IntentService;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Handler;
import android.webkit.CookieManager;

import com.elab.openbmsclient.R;
import com.elab.openbmsclient.receiver.ConnectivityReceiver;
import com.elab.openbmsclient.utility.display.DisplayToastRunnable;

import org.apache.http.HttpResponse;
import org.apache.http.client.CookieStore;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.cookie.Cookie;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

/**
 * Adrien Hoffet on 10.03.15.
 */
public class SendDynIDService extends IntentService {

    Handler mHandler;

    // Internal working strings
    public static final String IntentNameTextViewUpdater = "USER_REGISTERED_UPDATE_TV";
    public static final String IntentNameNewRoomIDUpdater = "ROOM_REGISTERED";
    public static final String IntentNameNewAccount = "USER_REGISTERED";
    public static final String ExtraNameNewUrl = "INTENT_NEW_URL";
    public static final String GlobalSharedParamName = "PrefFile"; // Name of the shared preference file
    public static final String SParamNameUserID = "UserID"; // Key for user ID in preferences
    public static final String SParamNameUser = "User"; // Key for user ID in preferences
    public static final String SParamNameUserIDCandidate = "UserIDCand"; // Key for user ID in preferences
    public static final String SParamUserCurrentRoom = "UserRoomID"; // Key for current room id of the user
    public static final String SParamServerHost = "ServeIP"; // The central server IP
    public static final String SParamLocUpdateInterval = "LocUpdateInterval"; // The location request interval
    public static final String SParamUrlPostQ = "URLPostQ";
    public static final String SParamUrlPutQ = "URLPushQ";
    public static final String ExtraNameID = "ID"; // Key for extra in the intent calling the service

    private static      String mServerAssociationUrl;
    private static      int mServerTimeout;
    private static      String mServerHost;
    public static final String SEPARATOR = ",";
    public static       boolean NextIsID = false;

    public SendDynIDService() {
        super(SendDynIDService.class.getName());

        mHandler = new Handler();
    }

    @Override
    public void onCreate() {
        super.onCreate();
        SharedPreferences settings = getSharedPreferences(GlobalSharedParamName, MODE_PRIVATE);

        String template_url = getResources().getString(R.string.server_association_url);
        mServerHost = settings.getString(SendDynIDService.SParamServerHost, getString(R.string.server_host_default)); //Get the address from store
        String api_sec_key = getResources().getString(R.string.api_sec_key);
        mServerAssociationUrl = String.format(template_url, mServerHost, api_sec_key);
        mServerTimeout = getResources().getInteger(R.integer.server_webview_timeout);
    }

    @Override
    protected void onHandleIntent(Intent intent) {

        // Retrieve the current ID
        String CurrentID =intent.getStringExtra(ExtraNameID);

        // Retrieve User ID from SharedPreferences
        SharedPreferences settings = getSharedPreferences(GlobalSharedParamName, MODE_PRIVATE);
        String UserID = settings.getString(SParamNameUserID, getString(R.string.DEFAULT_ID));

        boolean UserIDValid = !UserID.equals(getString(R.string.DEFAULT_ID)) && !UserID.equals("null");
        boolean CurrentIDValid = !CurrentID.equals(getString(R.string.DEFAULT_ID)) && !CurrentID.equals("null");

        // Test if the user ID is initialised
        if(!UserIDValid && !NextIsID && !ConnectivityReceiver.SendAll){

            //MainActivity.SetUserID();
            mHandler.post(new DisplayToastRunnable(this, getString(R.string.AlertRegisterUserBefore), 0));

        // ID Update
        }else if (CurrentIDValid && NextIsID) {

            // Try to send data to the server if available
            addToUserIDQ(CurrentID);
            sendUserID();

        // Send DynID
        }else if (UserIDValid && CurrentIDValid && !ConnectivityReceiver.SendAll){

            // Check if the Dynamic tag is not the user ID
            if(CurrentID.equals(UserID)){
                mHandler.post(new DisplayToastRunnable(this, getString(R.string.AlertCannotSentUserIDAsTag), 0));
            }else {
                addToDynIDQ(UserID, CurrentID);
                sendDynID();
            }
        }else if (ConnectivityReceiver.SendAll){
            sendUserID();
            sendDynID();
            ConnectivityReceiver.SendAll = false;
        }
    }

    // Save The personal ID to SharedPreferences
    void addToUserIDQ(String ID){
        SharedPreferences settings = getSharedPreferences(GlobalSharedParamName, MODE_PRIVATE);
        SharedPreferences.Editor editor = settings.edit();

        String uri = mServerAssociationUrl + ID;

        editor.putString(SParamNameUserIDCandidate, ID);
        editor.putString(SParamUrlPostQ, uri);
        editor.apply();
    }

    // Save The dynamic ID to SharedPreferences
    void addToDynIDQ(String UserID, String CurrentID){
        SharedPreferences settings = getSharedPreferences(GlobalSharedParamName, MODE_PRIVATE);
        SharedPreferences.Editor editor = settings.edit();

        String time = String.valueOf(System.currentTimeMillis() / 1000L);
        String uri = mServerAssociationUrl + UserID + "?dyn_uuid=" + CurrentID + ";timestamp=" + time;

        if(settings.getString(SParamUrlPutQ,null)!=null){
            editor.putString(SParamUrlPutQ, new StringBuilder().append(settings.getString(SParamUrlPutQ, null)).append(SEPARATOR).append(uri).toString());
        }else{
            editor.putString(SParamUrlPutQ, uri);
        }
        editor.apply();
    }

    void clearUserIDQ(){
        SharedPreferences settings = getSharedPreferences(GlobalSharedParamName, MODE_PRIVATE);

        settings.edit().remove(SParamUrlPostQ).apply();
    }

    void clearDynIDQ(){
        SharedPreferences settings = getSharedPreferences(GlobalSharedParamName, MODE_PRIVATE);

        settings.edit().remove(SParamUrlPutQ).apply();
    }

    void clearCandidate(){
        SharedPreferences settings = getSharedPreferences(GlobalSharedParamName, MODE_PRIVATE);

        settings.edit().remove(SParamNameUserIDCandidate).apply();
    }

    // Send the last personal id and all dynamic id and send back response for personal id in priority or for dyn id if there is no personal id
    void sendUserID(){
        HttpParams httpParameters = new BasicHttpParams();
        HttpConnectionParams.setConnectionTimeout(httpParameters, mServerTimeout);
        HttpConnectionParams.setSoTimeout(httpParameters, mServerTimeout);
        DefaultHttpClient httpClient = new DefaultHttpClient(httpParameters);

        String responseJsonPost = null;
        JSONObject jObject = null;
        JSONObject jObject_user = null;
        SharedPreferences settings = getSharedPreferences(GlobalSharedParamName, MODE_PRIVATE);
        SharedPreferences.Editor editor = settings.edit();
        boolean responseError = false;
        boolean sendError = false;
        String postQStr = settings.getString(SParamUrlPostQ, null);

        //Personal ID send
        if(postQStr!=null) {
            // Server Post
            HttpPost httppost = new HttpPost(postQStr);
            httppost.setHeader("User-Agent", getString(R.string.http_user_client));
            try {
                HttpResponse response = httpClient.execute(httppost);
                //Try to save the cookies to the WebViewAppApplication cookie store
                syncCookiesToAppCookieManager(postQStr, httpClient);
                responseJsonPost = EntityUtils.toString(response.getEntity(), "UTF-8");
            } catch (IOException e) {
                e.printStackTrace();
                responseJsonPost = null;
                sendError = true;
            }

            // Convert Response to JSON
            if(responseJsonPost != null) {
                try {
                    jObject = new JSONObject(responseJsonPost);
                    jObject_user = jObject.getJSONObject("User");
                } catch (JSONException e) {
                    e.printStackTrace();
                    jObject = null;
                    responseError = true;
                }
            }

            // Check Response
            if(jObject!=null){
                try{
                    String surname = jObject_user.getString("surname");
                    String name= jObject_user.getString("name");
                    Boolean status = jObject.getBoolean("Status");
                    String roomid = jObject_user.getString("current_location");

                    if(status) {
                        mHandler.post(new DisplayToastRunnable(this, getString(R.string.OKUserRegistered) + " " + name + " " + surname, 1));
                        String acceptedID = settings.getString(SParamNameUserIDCandidate, getString(R.string.DEFAULT_ID));
                        editor.putString(SParamNameUserID, acceptedID);
                        editor.putString(SParamNameUser, name + " " + surname);
                        editor.putString(SParamUserCurrentRoom, roomid);

                        //Broadcast the new url due to roomid change
                        int pos_with_roomid = getResources().getInteger(R.integer.pos_with_roomid);
                        String[] mUrlList = getResources().getStringArray(R.array.navigation_url_list);
                        String newurl = String.format(mUrlList[pos_with_roomid], mServerHost, roomid);
                        Intent update_url_intent = new Intent(IntentNameNewRoomIDUpdater);
                        update_url_intent.putExtra(ExtraNameNewUrl, newurl);
                        sendBroadcast(update_url_intent);
                        Intent update_account_intent = new Intent(IntentNameNewAccount);
                        sendBroadcast(update_account_intent);

                        //Update the view with the new user id
                        Intent updateTvIntent = new Intent(IntentNameTextViewUpdater);
                        updateTvIntent.putExtra(SParamNameUserID, acceptedID);
                        updateTvIntent.putExtra(SParamNameUser, name + " " + surname);
                        sendBroadcast(updateTvIntent);

                        NextIsID = false;
                        editor.apply();
                    }else{
                        mHandler.post(new DisplayToastRunnable(this, getString(R.string.AlertUserNotValid), 0));
                        NextIsID = true;
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    responseError = true;
                }
            }

            if(sendError){
                mHandler.post(new DisplayToastRunnable(this, getString(R.string.AlertUserRegistrationNetworkFail), 1));
            }else{
                clearUserIDQ();
                clearCandidate();
            }

            if(responseError){
                NextIsID = false;
                mHandler.post(new DisplayToastRunnable(this, getString(R.string.AlertUserRegistrationNetworkFail), 1));
            }
        }
    }

    // Send the last personal id and all dynamic id and send back response for personal id in priority or for dyn id if there is no personal id
    void sendDynID(){
        HttpParams httpParameters = new BasicHttpParams();
        HttpConnectionParams.setConnectionTimeout(httpParameters, mServerTimeout);
        HttpConnectionParams.setSoTimeout(httpParameters, mServerTimeout);
        DefaultHttpClient httpClient = new DefaultHttpClient(httpParameters);

        String responseJsonPut;
        JSONObject jObject = null;
        boolean answerError = false;
        boolean sendError = false;

        SharedPreferences settings = getSharedPreferences(GlobalSharedParamName, MODE_PRIVATE);
        SharedPreferences.Editor editor = settings.edit();
        String putQStr = settings.getString(SParamUrlPutQ, null);

        // Dynamic id
        if(putQStr!=null) {
            String[] putQ = putQStr.split(SEPARATOR);

            if (putQ.length > 1 && !putQ[1].equals("null"))
                mHandler.post(new DisplayToastRunnable(this, putQ.length + getString(R.string.NDynIDNeedToBeSenT), 2));

            for (String sPut : putQ) {

                if(!sPut.equals("null")) {
                    // Send ID
                    HttpPut httpput = new HttpPut(sPut);
                    httpput.setHeader("User-Agent", getString(R.string.http_user_client));
                    try {
                        HttpResponse response = httpClient.execute(httpput);
                        responseJsonPut = EntityUtils.toString(response.getEntity(), "UTF-8");
                    } catch (IOException e) {
                        e.printStackTrace();
                        responseJsonPut = null;
                        sendError = true;
                    }

                    // Convert Response
                    if(responseJsonPut != null) {
                        try {
                            jObject = new JSONObject(responseJsonPut);
                        } catch (JSONException e) {
                            e.printStackTrace();
                            jObject = null;
                            answerError = true;
                        }
                    }

                    // Check Response
                    if (jObject != null) {
                        try {
                            String status = jObject.getString("Status");

                            if (status.equals("true")) {
                                String type = jObject.getString("Type");
                                JSONObject jObject_room = jObject.getJSONObject(type);
                                String name = jObject_room.getString("name");
                                String roomid = jObject_room.getString("roomID");
                                editor.putString(SParamUserCurrentRoom, roomid);
                                editor.apply();
                                //Broadcast the new url due to roomid change
                                int pos_with_roomid = getResources().getInteger(R.integer.pos_with_roomid);
                                String[] mUrlList = getResources().getStringArray(R.array.navigation_url_list);
                                String newurl = String.format(mUrlList[pos_with_roomid], mServerHost, roomid);
                                Intent update_url_intent = new Intent(IntentNameNewRoomIDUpdater);
                                update_url_intent.putExtra(ExtraNameNewUrl, newurl);
                                sendBroadcast(update_url_intent);
                                String roomtype = jObject_room.getString("type");
                                mHandler.post(new DisplayToastRunnable(this, getString(R.string.OKWelcomeRoom)+ " " + roomtype + " " + name, 1));
                            } else {
                                mHandler.post(new DisplayToastRunnable(this, jObject.getString("StatusError"), 2));
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                            answerError = true;
                        }
                    } else
                        answerError = true;
                }
                if (answerError && !answerError)
                    mHandler.post(new DisplayToastRunnable(this, getString(R.string.AlertDynIDBadAnswer), 2));
            }

            if(sendError) {
                mHandler.post(new DisplayToastRunnable(this, getString(R.string.AlertDynIDNetworkFail), 1));
            }else{
                clearDynIDQ();
            }
        }
    }

    // Method to convert nfc id to string and inverting the bytes
    public static String getHex_inv(byte[] bytes) {
        StringBuilder sb = new StringBuilder();
        for (int i = bytes.length - 1; i >= 0; --i) {
            int b = bytes[i] & 0xff;
            if (b < 0x10)
                sb.append('0');
            sb.append(Integer.toHexString(b));
            if (i > 0)
                sb.append(":");
        }
        return sb.toString();
    }

    // Method to convert nfc id to string without inverting the bytes
    public static String getHex(byte[] bytes) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i <= bytes.length - 1; ++i) {
            int b = bytes[i] & 0xff;
            if (b < 0x10)
                sb.append('0');
            sb.append(Integer.toHexString(b));
            if (i < bytes.length - 1)
                sb.append(":");
        }
        return sb.toString();
    }

    //For sync the cookies across application
    private static void syncCookiesToAppCookieManager(String url, DefaultHttpClient httpClient) {

        CookieStore clientCookieStore = httpClient.getCookieStore();
        List<Cookie> cookies  = clientCookieStore.getCookies();
        if (cookies.size() < 1) return;

        CookieManager appCookieManager = CookieManager.getInstance();
        if (appCookieManager == null) return;

        //Extract any stored cookies for HttpClient CookieStore
        // Store this cookie header in Android app CookieManager
        URL url_obj = null;
        String host = null;
        try {
            url_obj = new URL(url);
            host = url_obj.getHost();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }

        for (Cookie cookie:cookies) {
            //HACK: Work around weird version-only cookies from cookie formatter.
            if (cookie.getName().equals("$Version")) break;

            String setCookieHeader = cookie.getName()+"="+cookie.getValue()+
                    "; Domain="+cookie.getDomain();
            appCookieManager.setCookie(host, setCookieHeader);
        }
    }
}