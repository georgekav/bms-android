package com.elab.openbmsclient.service;

import android.app.IntentService;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Handler;

import com.elab.openbmsclient.R;
import com.elab.openbmsclient.receiver.ConnectivityReceiver;
import com.elab.openbmsclient.utility.display.DisplayToastRunnable;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

/**
 * Adrien Hoffet on 29.04.15.
 */
public class SendGeofenceService extends IntentService {


    Handler mHandler;

    // Internal working strings
    public static final String ExtraNameFenceID = "fenceid"; // Key for extra in the intent calling the service
    public static final String ExtraNameFenceDirection = "fencedirection"; // Key for extra in the intent calling the service
    public static final String ExtraNameFenceType = "fencetype"; // Key for extra in the intent calling the service
    public static final String ExtraNameFenceLat = "fencelat"; // Key for extra in the intent calling the service
    public static final String ExtraNameFenceLon = "fencelon"; // Key for extra in the intent calling the service
    public static final String ExtraNameFenceAcc = "fenceAcc"; // Key for extra in the intent calling the service
    public static final String ExtraNameFenceRadius = "fenceRad"; // Key for extra in the intent calling the service

    public static final String SParamFenceUrlPutQ = "FenceURLPutQ";

    private static      String mServerLocationUrl;
    private static      int mServerTimeout;
    private static      String mServerHost;
    public static final String SEPARATOR = ",";

    public SendGeofenceService() {
        super(SendGeofenceService.class.getName());

        mHandler = new Handler();
    }

    @Override
    public void onCreate() {
        super.onCreate();
        SharedPreferences settings = getSharedPreferences(SendDynIDService.GlobalSharedParamName, MODE_PRIVATE);

        String template_url = getResources().getString(R.string.server_location_url);
        mServerHost = settings.getString(SendDynIDService.SParamServerHost, getString(R.string.server_host_default)); //Get the address from store
        String api_sec_key = getResources().getString(R.string.api_sec_key);
        mServerLocationUrl = String.format(template_url, mServerHost, api_sec_key);
        mServerTimeout = getResources().getInteger(R.integer.server_webview_timeout);
    }

    @Override
    protected void onHandleIntent(Intent intent) {

        // Retrieve the current ID
        String CurrentFenceID = intent.getStringExtra(ExtraNameFenceID);
        String CurrentFenceDirection = intent.getStringExtra(ExtraNameFenceDirection);
        String CurrentFenceType = intent.getStringExtra(ExtraNameFenceType);
        String CurrentFenceLat= intent.getStringExtra(ExtraNameFenceLat);
        String CurrentFenceLon = intent.getStringExtra(ExtraNameFenceLon);
        String CurrentFenceRad = intent.getStringExtra(ExtraNameFenceRadius);
        String CurrentFenceAcc = intent.getStringExtra(ExtraNameFenceAcc);

        // Retrieve User ID from SharedPreferences
        SharedPreferences settings = getSharedPreferences(SendDynIDService.GlobalSharedParamName, MODE_PRIVATE);
        String UserID = settings.getString(SendDynIDService.SParamNameUserID, getString(R.string.DEFAULT_ID));

        boolean UserIDValid = !UserID.equals(getString(R.string.DEFAULT_ID)) && !UserID.equals("null");

        // Test if the user ID is initialised
        if(!UserIDValid){

            //MainActivity.SetUserID();
            mHandler.post(new DisplayToastRunnable(this, getString(R.string.AlertRegisterUserBefore), 0));

        }else if (!ConnectivityReceiver.SendAll){

            addToFenceQ(UserID, CurrentFenceID, CurrentFenceDirection, CurrentFenceType, CurrentFenceLat, CurrentFenceLon, CurrentFenceAcc, CurrentFenceRad);
            sendFence();

        }else if (ConnectivityReceiver.SendAll){

            sendFence();
            ConnectivityReceiver.SendAll = false;
        }
    }

    // Save The personal ID to SharedPreferences
    void addToFenceQ(String UserID,
                     String CurrentFenceID,
                     String CurrentFenceDirection,
                     String CurrentFenceType,
                     String CurrentFenceLat,
                     String CurrentFenceLon,
                     String CurrentFenceAcc,
                     String Radius){
        SharedPreferences settings = getSharedPreferences(SendDynIDService.GlobalSharedParamName, MODE_PRIVATE);
        SharedPreferences.Editor editor = settings.edit();

        String time = String.valueOf(System.currentTimeMillis() / 1000L);
        String uri = mServerLocationUrl + UserID +
                "?fence_id=" + CurrentFenceID +
                ";location_type="  + CurrentFenceType +
                ";latitude="  + CurrentFenceLat +
                ";longitude="  + CurrentFenceLon +
                ";accuracy=" + CurrentFenceAcc +
                ";direction="  + CurrentFenceDirection +
                ";radius=" + Radius +
                ";timestamp=" + time;

        if(settings.getString(SParamFenceUrlPutQ,null)!=null){
            editor.putString(SParamFenceUrlPutQ, new StringBuilder().append(settings.getString(SParamFenceUrlPutQ, null)).append(SEPARATOR).append(uri).toString());
        }else{
            editor.putString(SParamFenceUrlPutQ, uri);
        }
        editor.apply();
    }

    void clearFenceQ(){
        SharedPreferences settings = getSharedPreferences(SendDynIDService.GlobalSharedParamName, MODE_PRIVATE);

        settings.edit().remove(SParamFenceUrlPutQ).apply();
    }

    // Send the last personal id and all dynamic id and send back response for personal id in priority or for dyn id if there is no personal id
    void sendFence(){
        HttpParams httpParameters = new BasicHttpParams();
        HttpConnectionParams.setConnectionTimeout(httpParameters, mServerTimeout);
        HttpConnectionParams.setSoTimeout(httpParameters, mServerTimeout);
        DefaultHttpClient httpClient = new DefaultHttpClient(httpParameters);

        String responseJsonPut;
        JSONObject jObject = null;
        boolean answerError = false;
        boolean sendError = false;

        SharedPreferences settings = getSharedPreferences(SendDynIDService.GlobalSharedParamName, MODE_PRIVATE);
        String putQStr = settings.getString(SParamFenceUrlPutQ, null);


        // Fence sending
        if(putQStr!=null) {
            String[] putQ = putQStr.split(SEPARATOR);

            if (putQ.length > 1 && !putQ[1].equals("null"))
                mHandler.post(new DisplayToastRunnable(this, putQ.length + getString(R.string.GeofenceNeedToBeSent), 2));

            for (String sPut : putQ) {

                if(!sPut.equals("null")) {
                    // Send Fence
                    HttpPut httpput = null;
                    try {
                        httpput = new HttpPut(sPut);
                        httpput.setHeader("User-Agent", getString(R.string.http_user_client));
                    }catch (IllegalArgumentException e){
                        sendError = true;
                    }

                    try {
                        HttpResponse response = httpClient.execute(httpput);
                        responseJsonPut = EntityUtils.toString(response.getEntity(), "UTF-8");
                    } catch (IOException e) {
                        e.printStackTrace();
                        responseJsonPut = null;
                        sendError = true;
                    }

                    // Convert Response
                    if(responseJsonPut != null) {
                        try {
                            jObject = new JSONObject(responseJsonPut);
                        } catch (JSONException e) {
                            e.printStackTrace();
                            jObject = null;
                            answerError = true;
                        }
                    }

                    // Check Response
                    if (jObject != null) {
                        try {
                            String status = jObject.getString("Status");

                            if (status.equals("true")) {
                                mHandler.post(new DisplayToastRunnable(this, "Status OK", 3));

                            } else {
                                mHandler.post(new DisplayToastRunnable(this, "Status OK", 3));
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                            answerError = true;
                        }
                    } else
                        answerError = true;
                }
                if (answerError && !answerError)
                    mHandler.post(new DisplayToastRunnable(this, getString(R.string.AlertDynIDBadAnswer), 2));
            }

            if(sendError) {
                mHandler.post(new DisplayToastRunnable(this, getString(R.string.AlertDynIDNetworkFail), 1));
            }else{
                clearFenceQ();
            }
        }
    }
}
