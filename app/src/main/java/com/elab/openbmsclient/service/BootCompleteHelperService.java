package com.elab.openbmsclient.service;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Handler;
import android.os.IBinder;

import com.elab.openbmsclient.R;
import com.elab.openbmsclient.utility.display.DisplayNotification;
import com.elab.openbmsclient.utility.geofences.GeofenceManager;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by adrienhoffet on 02.06.15.
 */
public class BootCompleteHelperService extends Service {
    Service mService = this;

    @Override
    public void onCreate() {
        super.onCreate();
    }


    @Override
    public int onStartCommand(final Intent intent, int flags, int startId) {
        final Context mContext = this.getApplicationContext();
        DisplayNotification.createNotification(mContext, "Boot Event", getString(R.string.GeofenceServiceStartNotification), 3);


        Handler h = new Handler();
        Runnable r1 = new Runnable() {
            @Override
            public void run() {
                SharedPreferences settings = mContext.getSharedPreferences(SendDynIDService.GlobalSharedParamName, Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = settings.edit();

                Boolean state = settings.getBoolean(GeofenceManager.SParamNameFenceState, false);

                if(state) {
                    DisplayNotification.createNotification(mContext, "Boot Event", getString(R.string.GeofenceRegisterNotification), 3);

                    List<String> isFenceRegistered = new ArrayList<>(Arrays.asList(settings.getString(GeofenceManager.SParamNameFenceReg, "").split(":")));

                    for (int i = 0; i < isFenceRegistered.size(); i++) {
                        isFenceRegistered.set(i, "false");
                    }

                    StringBuilder stringBuilderReg = new StringBuilder();
                    for (int i = 0; i < isFenceRegistered.size(); i++) {
                        stringBuilderReg.append(isFenceRegistered.get(i));
                        stringBuilderReg.append(":");
                    }

                    editor.putString(GeofenceManager.SParamNameFenceReg, stringBuilderReg.toString());
                    editor.apply();

                    GeofenceManager mGeofenceManager = new GeofenceManager(mContext);
                    mGeofenceManager.registerGeofences();
                    mService.stopSelf();
                }
                DisplayNotification.createNotification(mContext, "Boot Event", getString(R.string.GeofenceServiceStopNotification),3 );
            }
        };
        h.postDelayed(r1, 15000); // 15 second delay

        return START_NOT_STICKY;
    }

    @Override
    public IBinder onBind(Intent intent)
    {
        return null;
    }

}