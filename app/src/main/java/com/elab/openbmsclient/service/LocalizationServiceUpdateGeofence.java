package com.elab.openbmsclient.service;

import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.location.Location;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;
import android.widget.Toast;

import com.elab.openbmsclient.R;
import com.elab.openbmsclient.utility.display.DisplayNotification;
import com.elab.openbmsclient.utility.display.DisplayToast;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

public class LocalizationServiceUpdateGeofence
        extends Service
        implements GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener{

    private GoogleApiClient mGoogleApiClient;
    private LocationRequest mLocationRequest;
    private PendingIntent mProximityIntent;
    private boolean isOneShot = false;
    public static boolean isLocationUpdateRunning;
    private static final String IntentNameLocationUpdate = "location_update";
    public static final String IntentNameSingleLocationUpdate = "single_location_update";
    public static final String IntentNameStopLocationUpdate = "stop_location_update";
    public static final String ExtraNameRequestType = "request_type";
    public static final String ExtraNameLatitude = "latitude_value";
    public static final String ExtraNameLongitude = "longitude_value";
    public static final String ExtraNameAccuracy = "accuracy_value";
    public static final String ExtraRequestFenceRadius = "requested_fence_radius";
    public static final String TAG = LocalizationServiceUpdateGeofence.class.getSimpleName();
    private static int mRefreshRateLow;
    private static int updatesRadius;
    private static int currentFenceRadius;
    final static int mContinuousRefreshType = LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY;
    final static int mRefreshRateHigh = 1000;       // 1s used for poll of position. not for long term updates. For that is the configurable mRefreshRateLow

    @Override
    public void onCreate()
    {
        super.onCreate();
        SharedPreferences settings = getSharedPreferences(SendDynIDService.GlobalSharedParamName, MODE_PRIVATE);
        mRefreshRateLow = settings.getInt(SendDynIDService.SParamLocUpdateInterval, getResources().getInteger(R.integer.location_update_interval)); // 5min default - This is the service background refresh
        updatesRadius = getResources().getInteger(R.integer.location_update_radius);
        Log.i(TAG, "LocationService onCreate");
        isLocationUpdateRunning = false;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.i(TAG, "onStartCommand");
        //Setup the mLocationRequest object that specifies the location request type
        if(intent != null){
            if(intent.hasExtra(ExtraNameRequestType)) {
                Object extra = intent.getExtras().get(ExtraNameRequestType);
                if(extra.equals("Continuous")){
                    mLocationRequest = new LocationRequest()
                            .setInterval(mRefreshRateLow)
                            .setSmallestDisplacement(updatesRadius / 4) // 25m
                            .setPriority(mContinuousRefreshType);
                    isOneShot = false;
                }else if(extra.equals("Single")){
                    mLocationRequest = new LocationRequest()
                            .setInterval(mRefreshRateHigh) //Pull frequently until we get a good accuracy for fence center
                            .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
                    currentFenceRadius = Integer.parseInt(intent.getExtras().getString(ExtraRequestFenceRadius));
                    isOneShot = true;
                }
            }
        }else{ //If android killed the service and restarted, intent is null. Restart the listener
            mLocationRequest = new LocationRequest()
                    .setInterval(mRefreshRateLow)
                    .setSmallestDisplacement(updatesRadius / 4) // 25m
                    .setPriority(mContinuousRefreshType);
            isOneShot = false;
        }

        if (isLocationUpdateRunning & !isOneShot) {
            return START_STICKY;
        }
        else {
            //Google API client needs to be connected for the above location requests to work
            mGoogleApiClient = new GoogleApiClient.Builder(this)
                    .addApi(LocationServices.API)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .build();

            mGoogleApiClient.connect();

            registerReceiver(LocUpdater, new IntentFilter(IntentNameLocationUpdate));
            //The intent that the location updates with launch. See the function: startLocationUpdates()
            mProximityIntent = PendingIntent.getBroadcast(this, 0, new Intent(IntentNameLocationUpdate), PendingIntent.FLAG_CANCEL_CURRENT);

            registerReceiver(stopLocationUpdatesReceiver, new IntentFilter(IntentNameStopLocationUpdate));

            return START_STICKY;
        }
    }

    @Override
    public IBinder onBind(Intent intent)
    {
        Log.i(TAG, "onBind");

        return null;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.i(TAG, "onDestroy STOP SERVICE");
        isLocationUpdateRunning = false;
        try{unregisterReceiver(LocUpdater);}
        catch(IllegalArgumentException e)
        {//Not registered
        }
    }

    // *******************
    // *GOOGLE API CLIENT*
    // *******************
    @Override
    public void onConnected(Bundle bundle) {
        Log.i(TAG, "GoogleApiClient onConnected");

        startLocationUpdates();
    }

    @Override
    public void onConnectionSuspended(int i) {
        DisplayToast.createToast(this, getString(R.string.GoogleApiSuspendToast), Toast.LENGTH_SHORT, 3);
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        DisplayToast.createToast(this, getString(R.string.GoogleApiFailToast), Toast.LENGTH_SHORT, 3);
    }

    // *******************
    // *  Local Methods  *
    // *******************
    protected void startLocationUpdates() {
        Log.i(TAG, "startLocationUpdates");
        //Give to the API the Google API client object, the LocationRequest which specifies the parameters and the Intent to broadcast to.
        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, mProximityIntent);
        isLocationUpdateRunning = true;
    }

    protected void stopLocationUpdates() {
        Log.i(TAG, "stopLocationUpdates");

        LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, mProximityIntent);
        isLocationUpdateRunning = false;
        unregisterReceiver(stopLocationUpdatesReceiver);
        stopSelf();
    }

    private BroadcastReceiver stopLocationUpdatesReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Log.i(TAG, "BroadcastReceiver stopLocationUpdatesReceiver");
            stopLocationUpdates();
            DisplayToast.createToast(context, getString(R.string.LocationUpdatesStopToast), Toast.LENGTH_SHORT, 3);
        }
    };

    // Receiver to update the localisation
    private BroadcastReceiver LocUpdater= new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Log.i(TAG, "BroadcastReceiver LocUpdater");
            if (isOneShot) { //Else the Geofence will handle the continues updates and trigger intent when crosses the fence
                Location loc = (Location) intent.getExtras().get("com.google.android.location.LOCATION");

                if (loc != null){ //If GPS or Network did not resolve then we could have NullPointerException
                    double accu = loc.getAccuracy();
                    Log.i(TAG, "LocUpdater Accu "+accu);
                    if (accu < currentFenceRadius / 4) {
                        double lat = loc.getLatitude();
                        double lon = loc.getLongitude();

                        Intent update_loc_intent = new Intent(IntentNameSingleLocationUpdate);

                        update_loc_intent.putExtra(ExtraNameLatitude, lat);
                        update_loc_intent.putExtra(ExtraNameLongitude, lon);
                        update_loc_intent.putExtra(ExtraNameAccuracy, accu);

                        sendBroadcast(update_loc_intent);
                        stopLocationUpdates();
                        try {
                            unregisterReceiver(LocUpdater);
                        } catch (IllegalArgumentException e) {
                            e.printStackTrace();
                        }
                        stopSelf();
                        isOneShot = false;
                    }
                }
                else{
                    Log.i(TAG, "LocUpdater null");
                }

            }
            DisplayNotification.createNotification(context, "Location Event", getString(R.string.LocationUpdateTriggerNotification), 3);
        }
    };
}
