package com.elab.openbmsclient.utility.geofences;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.Location;
import android.os.Handler;
import android.support.v4.content.WakefulBroadcastReceiver;
import android.util.Log;
import android.widget.Toast;

import com.elab.openbmsclient.R;
import com.elab.openbmsclient.receiver.AirplaneModeReceiver;
import com.elab.openbmsclient.service.SendDynIDService;
import com.elab.openbmsclient.service.SendGeofenceService;
import com.elab.openbmsclient.utility.display.CheckLocationDisplayWirelessSettingsDialog;
import com.elab.openbmsclient.utility.display.DisplayNotification;
import com.elab.openbmsclient.utility.display.DisplayToast;
import com.google.android.gms.location.Geofence;
import com.google.android.gms.location.GeofencingEvent;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

public class GeofenceTransitionManager extends WakefulBroadcastReceiver {

    private static final String TAG = GeofenceTransitionManager.class.getSimpleName();

    private Context mContext;
    static int counter = 0;
    Handler mHandler;

    public GeofenceTransitionManager() {
        mHandler = new Handler();
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.i(TAG, "onReceive");
        this.mContext = context;
        GeofencingEvent event = GeofencingEvent.fromIntent(intent);

        SharedPreferences settings = context.getSharedPreferences(SendDynIDService.GlobalSharedParamName, Context.MODE_PRIVATE);
        String airplaneState = settings.getString(AirplaneModeReceiver.SParamNameAirplaneMode, "OFF");

        if(event != null & airplaneState.equals("OFF")){
            if(event.hasError()){
                onError(event.getErrorCode());
            } else {
                int transition = event.getGeofenceTransition();
                if(transition == Geofence.GEOFENCE_TRANSITION_ENTER || transition == Geofence.GEOFENCE_TRANSITION_DWELL || transition == Geofence.GEOFENCE_TRANSITION_EXIT){
                    String[] geofenceIds = new String[event.getTriggeringGeofences().size()];
                    for (int index = 0; index < event.getTriggeringGeofences().size(); index++) {
                        geofenceIds[index] = event.getTriggeringGeofences().get(index).getRequestId();
                    }

                    if (transition == Geofence.GEOFENCE_TRANSITION_ENTER || transition == Geofence.GEOFENCE_TRANSITION_DWELL) {
                        onEnteredGeofences(geofenceIds, event.getTriggeringLocation());
                    } else {
                        onExitedGeofences(geofenceIds, event.getTriggeringLocation());
                    }
                }
            }
        }else{
            DisplayToast.createToast(context, context.getString(R.string.GeofenceIgnoreAirplaneModeTriggerToast), Toast.LENGTH_SHORT, 3);
            Log.i(TAG, "Geofence trigger ignored due to airplane mode or reboot");

        }
    }

    protected void onEnteredGeofences(String[] geofenceIds, Location L) {
        Log.i(TAG, "onEnteredGeofence");

        for (String fenceId : geofenceIds) {
            DisplayToast.createToast(mContext, String.format(mContext.getString(R.string.FenceEnterToast), fenceId), Toast.LENGTH_SHORT, 2);
            Log.i(TAG, String.format("Entered fence: %1$s", fenceId));

            SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
            sdf.setTimeZone(TimeZone.getDefault());
            String currentTime = sdf.format(new Date());
            DisplayNotification.createNotification(mContext, "Fence Event", String.format(mContext.getString(R.string.FenceEnterNotification), fenceId, currentTime), 2);

            SharedPreferences settings = mContext.getSharedPreferences(SendDynIDService.GlobalSharedParamName, Context.MODE_PRIVATE);
            List<String> stringListFences = new ArrayList<>(Arrays.asList(settings.getString(GeofenceManager.SParamNameFenceID, "").split(":")));
            List<String> stringListRadius = new ArrayList<>(Arrays.asList(settings.getString(GeofenceManager.SParamNameFenceRadius, "").split(":")));
            List<String> stringListTypes = new ArrayList<>(Arrays.asList(settings.getString(GeofenceManager.SParamNameFenceType, "").split(":")));

            int currentID = 0;
            for(int index = 0; index < stringListFences.size(); index++){
                if(fenceId.equals(stringListFences.get(index))){
                    currentID = index;
                }
            }

            sendFenceUpdate(fenceId, "entered", L, stringListRadius.get(currentID), stringListTypes.get(currentID));
        }

    }

    protected void onExitedGeofences(String[] geofenceIds, Location L){
        Log.i(TAG, "onExitedGeofence");

        for (String fenceId : geofenceIds) {
            DisplayToast.createToast(mContext, String.format(mContext.getString(R.string.FenceExitToast), fenceId), Toast.LENGTH_SHORT, 2);
            Log.i(TAG, String.format("Exited fence: %1$s", fenceId));

            SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss", Locale.ENGLISH);
            sdf.setTimeZone(TimeZone.getDefault());
            String currentTime = sdf.format(new Date());
            DisplayNotification.createNotification(mContext, "Fence Event", String.format(mContext.getString(R.string.FenceExitNotification), fenceId, currentTime), 2);

            SharedPreferences settings = mContext.getSharedPreferences(SendDynIDService.GlobalSharedParamName, Context.MODE_PRIVATE);
            List<String> stringListFences = new ArrayList<>(Arrays.asList(settings.getString(GeofenceManager.SParamNameFenceID, "").split(":")));
            List<String> stringListRadiuss = new ArrayList<>(Arrays.asList(settings.getString(GeofenceManager.SParamNameFenceRadius, "").split(":")));
            List<String> stringListTypes = new ArrayList<>(Arrays.asList(settings.getString(GeofenceManager.SParamNameFenceType, "").split(":")));

            int currentID = 0;
            for(int index = 0; index < stringListFences.size(); index++){
                if(fenceId.equals(stringListFences.get(index))){
                    currentID = index;
                }
            }


            sendFenceUpdate(fenceId, "exited", L, stringListRadiuss.get(currentID), stringListTypes.get(currentID));
        }

    }

    protected void onError(int errorCode){
        DisplayToast.createToast(mContext, String.format("onError(%1$d)", errorCode), Toast.LENGTH_SHORT, 3);
        Log.e(TAG, String.format("onError(%1$d)", errorCode));
        mHandler.post(new CheckLocationDisplayWirelessSettingsDialog(mContext));
    }

    protected void sendFenceUpdate(String fenceID , String Direction, Location L, String Radius, String Type){

        final Intent SendGeofenceIntent = new Intent(mContext, SendGeofenceService.class);

        SendGeofenceIntent.putExtra(SendGeofenceService.ExtraNameFenceID,fenceID);
        SendGeofenceIntent.putExtra(SendGeofenceService.ExtraNameFenceDirection,Direction);
        SendGeofenceIntent.putExtra(SendGeofenceService.ExtraNameFenceType,Type);
        SendGeofenceIntent.putExtra(SendGeofenceService.ExtraNameFenceRadius,Radius);
        SendGeofenceIntent.putExtra(SendGeofenceService.ExtraNameFenceLat,String.format("%f",L.getLatitude()));
        SendGeofenceIntent.putExtra(SendGeofenceService.ExtraNameFenceLon,String.format("%f",L.getLongitude()));
        SendGeofenceIntent.putExtra(SendGeofenceService.ExtraNameFenceAcc,String.format("%f",L.getAccuracy()));

        mContext.startService(SendGeofenceIntent);
    }

}
