package com.elab.openbmsclient.utility.display;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;

import com.elab.openbmsclient.R;
import com.elab.openbmsclient.activity.MainActivity;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created by lilis on 05.06.2015.
 */
public class DisplayNotification {
    private final static AtomicInteger c = new AtomicInteger(0);

    private static int getID() {
        return c.incrementAndGet();
    }

    public static void createNotification(Context context, String title, String text, int notification_verbosity_lvl) {
        int app_verbosity_level = context.getResources().getInteger(R.integer.verbosity_level);
        if (app_verbosity_level >= notification_verbosity_lvl){
            NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(context);
            notificationBuilder.setAutoCancel(true).setDefaults(Notification.DEFAULT_ALL);
            notificationBuilder
                    .setContentText(text)
                    .setContentTitle(title)
                    .setSmallIcon(R.drawable.ic_notif_icon);
            //.setColor(Color.argb(0x55, 0x00, 0x00, 0xff));
            Intent notificationIntent = new Intent(context, MainActivity.class);
            notificationIntent.addCategory(Intent.CATEGORY_LAUNCHER);
            notificationIntent.setAction(Intent.ACTION_MAIN);
            NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
            PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, notificationIntent, 0);
            notificationBuilder.setContentIntent(pendingIntent);
            notificationManager.notify(getID(), notificationBuilder.build());
        }
    }
}
