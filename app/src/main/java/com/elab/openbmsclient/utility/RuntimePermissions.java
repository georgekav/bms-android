package com.elab.openbmsclient.utility;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by George on 28.02.2016.
 */
public class RuntimePermissions {
    private Context mContext;

    public static final int         request_permission_id_callback = 1;
    public static boolean           location_permission_granted = false;
    public static boolean           phone_permission_granted = false;
    public static boolean           storage_permission_granted = false;

    public RuntimePermissions(Context context){
        mContext=context;
    }

    public void requestPermissions(){
        //This is for Android M requesting permissions
        boolean request_necessary = false;
        List<String> request_list = new ArrayList<String>();

        if (ContextCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            request_necessary = true;
            request_list.add(Manifest.permission.ACCESS_FINE_LOCATION);
        }
        else{
            location_permission_granted = true;
        }
        if (ContextCompat.checkSelfPermission(mContext, Manifest.permission.READ_PHONE_STATE)
                != PackageManager.PERMISSION_GRANTED) {
            request_necessary = true;
            request_list.add(Manifest.permission.READ_PHONE_STATE);
        }
        else{
            phone_permission_granted = true;
        }
        if (ContextCompat.checkSelfPermission(mContext, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            request_necessary = true;
            request_list.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
        }
        else{
            storage_permission_granted = true;
        }

        if(request_necessary){
            String[] request_array = new String[ request_list.size() ];
            request_list.toArray( request_array );
            ActivityCompat.requestPermissions((Activity) mContext, request_array, request_permission_id_callback);
        }
    }

}
