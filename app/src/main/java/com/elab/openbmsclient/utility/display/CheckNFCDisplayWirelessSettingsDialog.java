package com.elab.openbmsclient.utility.display;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.nfc.NfcAdapter;
import android.provider.Settings;

import com.elab.openbmsclient.R;

/**
 * By adrienhoffet on 23.03.15.
 */
public class CheckNFCDisplayWirelessSettingsDialog implements Runnable{
    private final Context mContext;
    private NfcAdapter mNFCAdapter;

    public CheckNFCDisplayWirelessSettingsDialog(Context mContext){
        this.mContext = mContext;

    }

    public void run(){

        // Adapter to check if nfc is activated
        mNFCAdapter = NfcAdapter.getDefaultAdapter(mContext);

        // Check is NFC is on
        if (mNFCAdapter != null) {
            if (!mNFCAdapter.isEnabled()) {
                // Open Wireless Settings menu
                AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
                builder.setTitle("NFC settings");
                builder.setMessage(R.string.NFCDisabled);

                builder.setPositiveButton("Settings", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialogInterface, int i) {
                        Intent intent = new Intent(Settings.ACTION_WIRELESS_SETTINGS);
                        mContext.startActivity(intent);
                    }
                });
                builder.setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialogInterface, int i) {
                        //mContext.finish();
                    }
                });
                builder.create().show();
            }
        }
    }
}
