package com.elab.openbmsclient.utility.display;

import android.content.Context;
import android.widget.Toast;

import com.elab.openbmsclient.R;

/**
 * Created by lilis on 10.07.2015.
 */
public class DisplayToast {

    public static void createToast(Context context, String text, int toast_type, int notification_verbosity_lvl) {
        int app_verbosity_level = context.getResources().getInteger(R.integer.verbosity_level);
        if (app_verbosity_level >= notification_verbosity_lvl){
            Toast.makeText(context, text, toast_type).show();
        }
    }
}
