package com.elab.openbmsclient.utility.geofences;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.elab.openbmsclient.R;
import com.elab.openbmsclient.service.SendDynIDService;
import com.elab.openbmsclient.fragment.UserProfileFragment;
import com.elab.openbmsclient.service.LocalizationServiceUpdateGeofence;
import com.elab.openbmsclient.utility.display.DisplayToast;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.Geofence;
import com.google.android.gms.location.GeofencingRequest;
import com.google.android.gms.location.LocationServices;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class GeofenceManager
        implements GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener, ResultCallback<Status> {

    public static final String TAG = GeofenceManager.class.getSimpleName();
    Context mContext;
    private GoogleApiClient googleApiClient;
    private PendingIntent geofencePendingIntent;
    public List<Geofence> myFences = new ArrayList<>();
    Intent startServiceIntent;
    public static String remFence = "";
    public static List<String> fenceToUnregister;
    public static String PendingRegistering;
    private static boolean secondRegistering = false;
    public static final String SParamNameFenceLatitude = "FENCES_Lat";
    public static final String SParamNameFenceLongitude = "FENCES_Lon";
    public static final String SParamNameFenceID = "FENCES_ID";
    public static final String SParamNameFenceReg = "FENCES_Reg";
    public static final String SParamNameFenceType = "FENCES_Type";
    public static final String SParamNameFenceRadius = "FENCES_Radius";
    public static final String SParamNameFenceState = "FENCES_State";

    public GeofenceManager(Context context) {
        this.mContext = context;
        startServiceIntent = new Intent(context, LocalizationServiceUpdateGeofence.class);
    }

    // GOOGLE API CLIENT
    @Override
    public void onConnected(Bundle bundle) {
        Log.i(TAG, "onConnected");
        // Set the Transition manager
        if (geofencePendingIntent == null) {
            Intent intent = new Intent(mContext, GeofenceTransitionManager.class);
            intent.setAction("geofence_transition_action");
            geofencePendingIntent = PendingIntent.getBroadcast(mContext, R.id.geofence_transition_intent, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        }

        if (remFence.equals("ALL") & fenceToUnregister == null) {
            removeFenceUtil();
            remFence = "";
        } else if (!remFence.equals("") & fenceToUnregister == null) {
            removeFenceUtil(remFence);
            remFence = "";
        } else if (secondRegistering) {
            secondRegistering = false;
            registerGeofences();
        }
        if (fenceToUnregister != null) {
            unRegisterGeofences(fenceToUnregister);
        }
    }

    @Override
    public void onConnectionSuspended(int i) {
        DisplayToast.createToast(mContext, mContext.getString(R.string.GoogleApiSuspendToast), Toast.LENGTH_SHORT, 3);
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        DisplayToast.createToast(mContext, mContext.getString(R.string.GoogleApiFailToast), Toast.LENGTH_SHORT, 3);
    }

    @Override
    public void onResult(Status status) {
        Log.i(TAG, "onResult");

        String toastMessage;
        if (status.isSuccess()) {
            toastMessage = mContext.getString(R.string.MonitoringFencesSuccessToast);

            SharedPreferences settings = mContext.getSharedPreferences(SendDynIDService.GlobalSharedParamName, Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = settings.edit();

            editor.putString(SParamNameFenceReg, PendingRegistering);
            editor.apply();

        } else {
            toastMessage = mContext.getString(R.string.MonitoringFencesErrorToast);
        }
        DisplayToast.createToast(mContext, toastMessage, Toast.LENGTH_SHORT, 3);

        UserProfileFragment.updateView();
    }

    public void connectAPI() {
        Log.i(TAG, "connectAPI");

        googleApiClient = new GoogleApiClient.Builder(mContext)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();
        googleApiClient.connect();
    }

    public void addFence(double newFenceLat, double newFenceLon, double acc, String newFenceID, String fenceType, String fenceRadius) {
        String creation_msg = "Fence Created: " + newFenceID + " (" + String.format("%.3f", newFenceLat)
                + "," + String.format("%.3f", newFenceLon) + ")\n type = " + fenceType + ", radius = "
                + fenceRadius + ", acc = " + String.format("%.0f", acc) + "m";
        Log.i(TAG, "addFence: "+creation_msg);
        DisplayToast.createToast(mContext, creation_msg, Toast.LENGTH_LONG, 1);
        SharedPreferences settings = mContext.getSharedPreferences(SendDynIDService.GlobalSharedParamName, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = settings.edit();

        String registeredFencesID = settings.getString(SParamNameFenceID, "");
        StringBuilder stringBuilderID = new StringBuilder();
        stringBuilderID.append(registeredFencesID);
        stringBuilderID.append(newFenceID);
        stringBuilderID.append(":");
        editor.putString(SParamNameFenceID, stringBuilderID.toString());

        String registeredFencesLat = settings.getString(SParamNameFenceLatitude, "");
        StringBuilder stringBuilderLat = new StringBuilder();
        stringBuilderLat.append(registeredFencesLat);
        stringBuilderLat.append(newFenceLat);
        stringBuilderLat.append(":");
        editor.putString(SParamNameFenceLatitude, stringBuilderLat.toString());

        String registeredFencesLon = settings.getString(SParamNameFenceLongitude, "");
        StringBuilder stringBuilderLon = new StringBuilder();
        stringBuilderLon.append(registeredFencesLon);
        stringBuilderLon.append(newFenceLon);
        stringBuilderLon.append(":");
        editor.putString(SParamNameFenceLongitude, stringBuilderLon.toString());

        String registeredFencesState = settings.getString(SParamNameFenceReg, "");
        StringBuilder stringBuilderState = new StringBuilder();
        stringBuilderState.append(registeredFencesState);
        stringBuilderState.append("false");
        stringBuilderState.append(":");
        editor.putString(SParamNameFenceReg, stringBuilderState.toString());

        String registeredFencesType = settings.getString(SParamNameFenceType, "");
        StringBuilder stringBuilderType = new StringBuilder();
        stringBuilderType.append(registeredFencesType);
        stringBuilderType.append(fenceType);
        stringBuilderType.append(":");
        editor.putString(SParamNameFenceType, stringBuilderType.toString());

        String registeredFencesRadius = settings.getString(SParamNameFenceRadius, "");
        StringBuilder stringBuilderRadius = new StringBuilder();
        stringBuilderRadius.append(registeredFencesRadius);
        stringBuilderRadius.append(fenceRadius);
        stringBuilderRadius.append(":");
        editor.putString(SParamNameFenceRadius, stringBuilderRadius.toString());

        editor.apply();

        if (settings.getBoolean(SParamNameFenceState, false)) {
            registerGeofences();
        }

        UserProfileFragment.updateView();
    }

    public void registerGeofences() {
        if (googleApiClient != null) {
            SharedPreferences settings = mContext.getSharedPreferences(SendDynIDService.GlobalSharedParamName, Context.MODE_PRIVATE);

            List<String> stringListFence = new ArrayList<>(Arrays.asList(settings.getString(SParamNameFenceID, "").split(":")));
            List<String> stringListLat = new ArrayList<>(Arrays.asList(settings.getString(SParamNameFenceLatitude, "").split(":")));
            List<String> stringListLon = new ArrayList<>(Arrays.asList(settings.getString(SParamNameFenceLongitude, "").split(":")));
            List<String> isFenceRegistered = new ArrayList<>(Arrays.asList(settings.getString(SParamNameFenceReg, "").split(":")));
            List<String> stringRadius = new ArrayList<>(Arrays.asList(settings.getString(SParamNameFenceRadius, "").split(":")));

            for (int i = 0; i < stringListFence.size(); i++) {
                if (isFenceRegistered.get(i).equals("false")) {
                    double mlat = Double.parseDouble(stringListLat.get(i));
                    double mlon = Double.parseDouble(stringListLon.get(i));
                    float mrad = Float.parseFloat(stringRadius.get(i));

                    Geofence geofence = new Geofence.Builder()
                            .setCircularRegion(mlat, mlon, mrad)
                            .setRequestId(stringListFence.get(i))
                            .setTransitionTypes(Geofence.GEOFENCE_TRANSITION_ENTER | Geofence.GEOFENCE_TRANSITION_EXIT)
                            .setExpirationDuration(Geofence.NEVER_EXPIRE)
                            .build();
                    //myFences.clear();
                    myFences.add(geofence);

                    isFenceRegistered.set(i, "true");
                }
            }

            StringBuilder stringBuilderReg = new StringBuilder();
            for (int i = 0; i < isFenceRegistered.size(); i++) {
                stringBuilderReg.append(isFenceRegistered.get(i));
                stringBuilderReg.append(":");
            }
            PendingRegistering = stringBuilderReg.toString();


            // Register the geofence array
            if (!myFences.isEmpty()) {
                LocationServices.GeofencingApi.addGeofences(
                        googleApiClient,
                        getGeoFencingRequest(),
                        getGeofencePendingIntent()
                ).setResultCallback(this);
                myFences.clear();

                if (!LocalizationServiceUpdateGeofence.isLocationUpdateRunning) {
                    startServiceIntent.putExtra(LocalizationServiceUpdateGeofence.ExtraNameRequestType, "Continuous");
                    mContext.startService(startServiceIntent);
                }
            }else{
                DisplayToast.createToast(mContext, mContext.getString(R.string.FencesActivationErrorToast), Toast.LENGTH_SHORT, 2);
            }

        } else {
            secondRegistering = true;
            connectAPI();
        }
    }

    private PendingIntent getGeofencePendingIntent() {
        // Reuse the PendingIntent if we already have it.
        if (geofencePendingIntent != null) {
            return geofencePendingIntent;
        }
        Intent intent = new Intent(mContext, GeofenceTransitionManager.class);
        // We use FLAG_UPDATE_CURRENT so that we get the same pending intent back when
        // calling addGeofences() and removeGeofences().
        return PendingIntent.getService(mContext, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
    }

    private GeofencingRequest getGeoFencingRequest() {
        GeofencingRequest.Builder builder = new GeofencingRequest.Builder();
        builder.setInitialTrigger(0); //No triggering when adding a fence and we are inside. Good for airplane mode
        builder.addGeofences(myFences);
        return builder.build();
    }

    public void unRegisterAllGeofences(){
        SharedPreferences settings = mContext.getSharedPreferences(SendDynIDService.GlobalSharedParamName, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = settings.edit();

        String registeredFences = settings.getString(SParamNameFenceID, "");
        String[] separatedFences = registeredFences.split(":");
        List<String> stringList = new ArrayList<>(Arrays.asList(separatedFences));
        unRegisterGeofences(stringList);

        List<String> isFenceRegistered = new ArrayList<>(Arrays.asList(settings.getString(SParamNameFenceReg, "").split(":")));

        StringBuilder stringBuilderReg = new StringBuilder();
        for(int i = 0; i< isFenceRegistered.size();i++){
            stringBuilderReg.append("false");
            stringBuilderReg.append(":");
        }

        editor.putString(SParamNameFenceReg, stringBuilderReg.toString());
        editor.apply();
    }

    public void unRegisterGeofences(List<String> temp){
        Log.i(TAG, "removeFence");
        if(googleApiClient == null){
            connectAPI();
            remFence = "";
            fenceToUnregister = temp;
        }else {
            LocationServices.GeofencingApi.removeGeofences(googleApiClient, temp);
            DisplayToast.createToast(mContext, String.format(mContext.getString(R.string.GeofenceUnregisterToast), temp), Toast.LENGTH_SHORT, 2);
            fenceToUnregister = null;
        }
    }

    public void removeFence(){
        Log.i(TAG, "removeFence");
        if(googleApiClient == null){
            connectAPI();
            remFence = "ALL";
        }else{
            unRegisterAllGeofences();
            removeFenceUtil();
            //LocationServices.GeofencingApi.removeGeofences(googleApiClient, geofencePendingIntent);
        }
    }

    public void removeFence(String fence){
        Log.i(TAG, "removeFence param: " + fence);
        if(googleApiClient == null){
            connectAPI();
            remFence = fence;
        }else{
            removeFenceUtil(fence);
            //LocationServices.GeofencingApi.removeGeofences(googleApiClient, geofencePendingIntent);
        }
    }

    public void removeFenceUtil(){
        Log.i(TAG, "removeFenceUtil");
        SharedPreferences settings = mContext.getSharedPreferences(SendDynIDService.GlobalSharedParamName, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = settings.edit();

        editor.putString(SParamNameFenceID, "");
        editor.putString(SParamNameFenceLatitude, "");
        editor.putString(SParamNameFenceLongitude, "");
        editor.putString(SParamNameFenceReg, "");
        editor.putString(SParamNameFenceRadius, "");
        editor.putString(SParamNameFenceType, "");

        editor.apply();

        UserProfileFragment.updateView();
    }


    public void removeFenceUtil(String fence){
        Log.i(TAG, "removeFenceUtil");
        SharedPreferences settings = mContext.getSharedPreferences(SendDynIDService.GlobalSharedParamName, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = settings.edit();

        List<String> fenceToSearch = new ArrayList<>(Arrays.asList(fence.split(" ")));

        List<String> stringListFence = new ArrayList<>(Arrays.asList(settings.getString(SParamNameFenceID, "").split(":")));
        List<String> stringListLat = new ArrayList<>(Arrays.asList(settings.getString(SParamNameFenceLatitude, "").split(":")));
        List<String> stringListLon = new ArrayList<>(Arrays.asList(settings.getString(SParamNameFenceLongitude, "").split(":")));
        List<String> stringListReg = new ArrayList<>(Arrays.asList(settings.getString(SParamNameFenceReg, "").split(":")));
        List<String> stringListRadius = new ArrayList<>(Arrays.asList(settings.getString(SParamNameFenceRadius, "").split(":")));
        List<String> stringListType = new ArrayList<>(Arrays.asList(settings.getString(SParamNameFenceType, "").split(":")));
        List<String> stringListNewFence = new ArrayList<>();
        List<String> stringListNewLat = new ArrayList<>();
        List<String> stringListNewLon = new ArrayList<>();
        List<String> stringListNewReg = new ArrayList<>();
        List<String> stringListNewRadius = new ArrayList<>();
        List<String> stringListNewType = new ArrayList<>();

        for(int i = 0; i< stringListFence.size(); i++){
            if(stringListFence.get(i).equals(fenceToSearch.get(0))){
                if(stringListReg.get(i).equals("true")) {
                    List<String> temp = new ArrayList<>(Arrays.asList(stringListFence.get(i)));
                    unRegisterGeofences(temp);
                    DisplayToast.createToast(mContext, String.format(mContext.getString(R.string.GeofenceDeleteToast), temp), Toast.LENGTH_SHORT, 3);
                }
            }else{
                stringListNewFence.add(stringListFence.get(i));
                stringListNewLat.add(stringListLat.get(i));
                stringListNewLon.add(stringListLon.get(i));
                stringListNewReg.add(stringListReg.get(i));
                stringListNewRadius.add(stringListRadius.get(i));
                stringListNewType.add(stringListType.get(i));
            }
        }

        StringBuilder stringBuilderFence = new StringBuilder();
        StringBuilder stringBuilderLat = new StringBuilder();
        StringBuilder stringBuilderLon = new StringBuilder();
        StringBuilder stringBuilderReg = new StringBuilder();
        StringBuilder stringBuilderRadius = new StringBuilder();
        StringBuilder stringBuilderType = new StringBuilder();
        for(int i = 0; i< stringListNewFence.size();i++){
            stringBuilderFence.append(stringListNewFence.get(i));
            stringBuilderFence.append(":");
            stringBuilderLat.append(stringListNewLat.get(i));
            stringBuilderLat.append(":");
            stringBuilderLon.append(stringListNewLon.get(i));
            stringBuilderLon.append(":");
            stringBuilderReg.append(stringListNewReg.get(i));
            stringBuilderReg.append(":");
            stringBuilderType.append(stringListNewType.get(i));
            stringBuilderType.append(":");
            stringBuilderRadius.append(stringListNewRadius.get(i));
            stringBuilderRadius.append(":");
        }

        editor.putString(SParamNameFenceID, stringBuilderFence.toString());
        editor.putString(SParamNameFenceLatitude, stringBuilderLat.toString());
        editor.putString(SParamNameFenceLongitude, stringBuilderLon.toString());
        editor.putString(SParamNameFenceReg, stringBuilderReg.toString());
        editor.putString(SParamNameFenceRadius, stringBuilderRadius.toString());
        editor.putString(SParamNameFenceType, stringBuilderType.toString());

        editor.apply();

        //LocationServices.GeofencingApi.removeGeofences(googleApiClient, stringListNew);
        UserProfileFragment.updateView();
    }

    public void disconnectAPI(){
        if(googleApiClient!= null) {
            googleApiClient.disconnect();
            googleApiClient = null;
        }
    }
}