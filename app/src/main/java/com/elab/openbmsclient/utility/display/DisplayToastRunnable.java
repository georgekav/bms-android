package com.elab.openbmsclient.utility.display;

import android.content.Context;
import android.widget.Toast;

import com.elab.openbmsclient.R;

/**
 * Created by lilis on 10.07.2015.
 */
public class DisplayToastRunnable implements Runnable {
    private final Context mContext;
    private String mText;
    private int notif_verb_lvl;
    private int app_verb_lvl;

    public DisplayToastRunnable(Context mContext, String text, int notification_verbosity_lvl){
        this.mContext = mContext;
        mText = text;
        notif_verb_lvl = notification_verbosity_lvl;
        app_verb_lvl = mContext.getResources().getInteger(R.integer.verbosity_level);
    }

    public void run(){
        if (app_verb_lvl >= notif_verb_lvl) {
            Toast.makeText(mContext, mText, Toast.LENGTH_SHORT).show();
        }
    }
}
