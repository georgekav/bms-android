package com.elab.openbmsclient.utility.display;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;

import com.elab.openbmsclient.R;
import com.elab.openbmsclient.fragment.UserProfileFragment;
import com.elab.openbmsclient.service.SendDynIDService;

/**
 * by adrien hoffet on 23.03.15.
 */
public class DisplaySetUserIDDialog implements Runnable{
    private final Context mContext;

    public DisplaySetUserIDDialog(Context mContext){
        this.mContext = mContext;

    }

    public void run(){
        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        builder.setTitle(R.string.UserProfileAskUUIDTitle);
        builder.setMessage(R.string.UserProfileAskUUIDMessage);

        builder.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener()
        {
            public void onClick(DialogInterface dialogInterface, int i) {
                SharedPreferences settings = mContext.getSharedPreferences(SendDynIDService.GlobalSharedParamName, Context.MODE_PRIVATE);
                settings.edit().remove(SendDynIDService.SParamNameUserID).apply();
                settings.edit().remove(SendDynIDService.SParamNameUser).apply();

                Intent updateTvIntent = new Intent(SendDynIDService.IntentNameTextViewUpdater);
                updateTvIntent.putExtra(SendDynIDService.SParamNameUserID,mContext.getString(R.string.DEFAULT_ID));
                updateTvIntent.putExtra(SendDynIDService.SParamNameUser,mContext.getString(R.string.DEFAULT_ID));
                mContext.sendBroadcast(updateTvIntent);

                SendDynIDService.NextIsID = true;
            }
        });

        builder.setNegativeButton(R.string.UserProfileCancelbutton, new DialogInterface.OnClickListener()
        {
            public void onClick(DialogInterface dialogInterface, int i) {
                SharedPreferences settings = mContext.getSharedPreferences(SendDynIDService.GlobalSharedParamName, Context.MODE_PRIVATE);
                settings.edit().remove(SendDynIDService.SParamNameUserID).apply();
                settings.edit().remove(SendDynIDService.SParamNameUser).apply();

                Intent updateTvIntent = new Intent(SendDynIDService.IntentNameTextViewUpdater);
                updateTvIntent.putExtra(SendDynIDService.SParamNameUserID,mContext.getString(R.string.DEFAULT_ID));
                updateTvIntent.putExtra(SendDynIDService.SParamNameUser,mContext.getString(R.string.DEFAULT_ID));
                mContext.sendBroadcast(updateTvIntent);

                SendDynIDService.NextIsID = false;

                UserProfileFragment.switch1.setChecked(false);
            }
        });

        builder.create().show();
    }
}