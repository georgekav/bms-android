package com.elab.openbmsclient;

import android.app.Application;
import android.content.Context;
import android.webkit.CookieManager;


public class WebViewAppApplication extends Application
{
	private static WebViewAppApplication mInstance;

	public WebViewAppApplication()
	{
		mInstance = this;
	}


	@Override
	public void onCreate()
	{
		super.onCreate();

		// force AsyncTask to be initialized in the main thread due to the bug:
		// http://stackoverflow.com/questions/4280330/onpostexecute-not-being-called-in-asynctask-handler-runtime-exception
		try{
			Class.forName("android.os.AsyncTask");
		}
		catch(ClassNotFoundException e){
			e.printStackTrace();
		}
        try{
            CookieManager.getInstance().setAcceptCookie(true);
        }
        catch(Exception e) {
        }
	}


	public static Context getContext()
	{
        return mInstance;
	}

}
