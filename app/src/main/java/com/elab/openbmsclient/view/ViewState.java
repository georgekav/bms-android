package com.elab.openbmsclient.view;


public enum ViewState
{
	CONTENT, PROGRESS, OFFLINE, EMPTY
}
